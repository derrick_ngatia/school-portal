<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscplinaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discplinary', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned();
            $table->String('offense');
            $table->String('discplinary_action');
            $table->date('app_date');
            $table->boolean('cleared');
            $table->timestamps();
        });
        Schema::table('discplinary', function($table) {
      $table->foreign('student_id')->references('id')->on('students');
  });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discplinary');
    }
}
