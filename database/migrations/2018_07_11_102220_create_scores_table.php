<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('s_id');
            $table->integer('class');
            $table->integer('term');
            $table->integer('math');
            $table->integer('eng');
            $table->integer('kisw');
            $table->integer('sci');
            $table->integer('ss');
            $table->integer('total');
            $table->string('mteacher');
            $table->string('eteacher');
            $table->string('steacher');
            $table->string('kteacher');
            $table->string('sciteacher');
            $table->string('class_teachers_comment')->nullable();
            $table->string('parents_comment')->nullable();
            $table->string('dPrincipals_comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scores');
    }
}
