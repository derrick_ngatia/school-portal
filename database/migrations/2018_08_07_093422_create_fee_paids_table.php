<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeePaidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_paids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('s_admission');
            $table->string('acc_ref');
            $table->string('acc_no');
            $table->string('bank_name');
            $table->string('acc_name');
            $table->string('phone');
            $table->integer('amount');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_paids');
    }
}
