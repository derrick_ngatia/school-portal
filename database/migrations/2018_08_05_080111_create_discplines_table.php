<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscplinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discplines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('s_id');
            $table->integer('class');
            $table->integer('term');
            $table->text('offense');
            $table->text('disciplinary_action');
            $table->text('warning');
            $table->boolean('cleared');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discplines');
    }
}
