<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performance', function (Blueprint $table) {
            $table->integer('student_no')->unsigned();
            $table->String('exam_name');
            $table->integer('year');
            $table->integer('term');
            $table->integer('position');
            $table->String('score');
            $table->timestamps();
            $table->foreign('student_no')->references('id')->on('students');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performance');
    }
}
