<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecretariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secretaries', function (Blueprint $table) {
            $table->increments('id');
            $table->double('class_1');
            $table->double('class_2');
            $table->double('class_3');
            $table->double('class_4');
            $table->double('class_5');
            $table->double('class_6');
            $table->double('class_7');
            $table->double('class_8');
            $table->text('notes');
            $table->double('enrollment');
            $table->double('caution');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('secretaries');
    }
}
