<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  //  return view('welcome');
  return View('auth.login');
});
Route::get('/aboutus', function () {
  //  return view('welcome');
  return View('aboutus');

});
Route::get('/error',function (){
    return view('error');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('class_teacher','ClassTeacherController@index')->name('home');
Route::get('headteacher','HeadTeacherController@index');
Route::get('/fetchUnread/{id}', 'MessagesController@fetchUnread');
Route::get('/getMessages/{id}','MessagesController@store');
Route::post('/sendmessage/{id}','MessagesController@store1');
Route::post('/create_message/{id}','MessagesController@store');
Route::get('/getuid','MessagesController@id');
Route::get('/help','Controller@help');
Route::post('/submit','Controller@post');




Auth::routes();
Route::middleware(['auth'])->group(function (){


Route::group(['middleware'=>'parents'],function () {
    Route::get('/chat','ParentsController@chat');
    Route::get('/p_results','ParentsController@child');
    Route::get('/fee_paid','ParentsController@fee_paid');
    Route::get('/results/{id}','ParentsController@results');
    Route::get('/fee/{id}','ParentsController@fee');
    Route::get('/disciplinary/{id}','ParentsController@disciplinary');
    Route::get('/parent_student_discipline','ParentsController@mykids');
    Route::get('/print_fee_receipt/{id}','ParentsController@fee_receipt');
    Route::get('/parentshome','ParentsController@index');
    Route::get('/print_report_form_score/{id}','ParentsController@print_report_form_score');
    Route::get('/print_fee_structure_parent','ParentsController@print_fee_structure_parent');
    Route::get('/fee_structure_parent','ParentsController@fee_structure');
    Route::get('/print_report_form/{id}','SecretaryController@student_report_form');


});
    Route::group(['middleware'=>'administrator'],function () {

    Route::resource('administration','AdminController');
    Route::get('/performance','performancecontroller@perfomance');
    Route::get('/update/{id}','AdminController@update_parent');
    Route::post('/updateParentAccount/{id}','AdminController@update_a');
    Route::get('/editAccount','AdminController@edit_parent');
    Route::post('/news','performancecontroller@news');
    Route::get('/score','performancecontroller@addscore');
    Route::post('/addscore','performancecontroller@postscore');
    Route::post('/newsupdate/{id}','performancecontroller@newsupdate');
    Route::get('/parentAccount','AdminController@parentForm');
    Route::get('/add','AdminController@getStudentForm');
    Route::get('/viewstudents','AdminController@viewStudents');
    Route::get('/teacher','AdminController@addTeacher');
    Route::get('/classteachers','AdminController@getAll');
    Route::get(' /assignClassteachers','AdminController@assignClassTeacher');
    Route::post(' /assignTeacher','AdminController@assignTeacher');
    Route::post('/addStudent','AdminController@store');
    Route::post('/registerTeacher','AdminController@storeTeacher');
    Route::post('/register_secretary','AdminController@secretary');
    Route::post('/register_head_teacher','AdminController@head_teacher');

        Route::post('/createParentAccount','AdminController@parentAccount');
        Route::get('/add_head_teacher','AdminController@add_head_teacher');
        Route::get('/add_secretary','AdminController@add_secretary');
        Route::get('/delete/{id}','HomeController@delete');
    Route::get('/edit/{id}','HomeController@edit');
    Route::get('/deleteAccount','AdminController@delete');
    Route::get('/deletesAccount/{id}','AdminController@deleteAccount');
    Route::post('/search_class_teacher','AdminController@search_class_teacher');
    Route::post('/search_student','AdminController@search_student');
    Route::post('/search_parent_account','AdminController@search_parent_account');



        //update class teacher
    Route::get('/update-class-teacher/{staff_no}','AdminController@edit');
    Route::post('/update-class-teachers/{id}','AdminController@update');
    Route::get('/delete-class-teacher/{id}','AdminController@destroy');
});



Route::group(['middleware'=>'class_teacher'],function (){
    //class teacher routes
    Route::get('/view-students','ClassTeacherController@show');
    Route::get('/recordMarks/{id}','ClassTeacherController@record');
    Route::get('/view_marks/{id}','ClassTeacherController@view_marks');
    Route::post('/calculate','ClassTeacherController@calculate');
    Route::get('/student_results','ClassTeacherController@student_results');
    Route::post('/store_score/{id}','ClassTeacherController@store_score');
    Route::get('/editscore/{student_id}/{score_id}','ClassTeacherController@update');
    Route::post('/update_score/{studentid}/{scoreid}','ClassTeacherController@store');
});

Route::group(['middleware'=>'head_teacher'],function () {
    Route::post('/search_student_c','HeadTeacherController@search_student_c');
    Route::post('/search_student_h','HeadTeacherController@search_student');
    Route::post('/register_case','HeadTeacherController@register_case');

Route::get('/schools_events','HeadTeacherController@schools_events');

Route::get('/school_disciplinary_actions','HeadTeacherController@school_disciplinary_actions');
    Route::post('/head_teacher_comment/{id}','HeadTeacherController@head_teacher_comment');
Route::get('/student_profile/{id}','HeadTeacherController@profile');
Route::get('/clear_case/{id}/{c_id}','HeadTeacherController@clear_case');
Route::get('/disciplinary_pending_cases','HeadTeacherController@pending');
Route::get('/register_case_disciplinary','HeadTeacherController@case_form');

});
Route::group(['middleware'=>'secretary'],function (){
    Route::get('/secretary','SecretaryController@index');
    Route::get('/secretary1','SecretaryController@parents');
    Route::get('/fee_payment','SecretaryController@fee_payment');
    Route::post('/record_fee_payment','SecretaryController@record_fee_payment');
    Route::post('/search_report_form','SecretaryController@search_report_form');
    Route::post('/generate/{id}','SecretaryController@generate');
    Route::get('/generate_report_forms','SecretaryController@report_forms');
    Route::get('/news','SecretaryController@news');
    Route::get('/news_letter','SecretaryController@news_letter');
    Route::get('/fee_structure','SecretaryController@fee_structure');
    Route::post('post_fee_structure','SecretaryController@post_fee_structure');
    Route::post('update_fee_structure','SecretaryController@update_fee_structure');
    Route::get('/print_report_form/{id}','SecretaryController@student_report_form');
    Route::get('/edit_fee_structure','SecretaryController@edit_fee_structure');
    Route::get('/sendMessage/{id}','SecretaryController@message_form');

});
});





?>
