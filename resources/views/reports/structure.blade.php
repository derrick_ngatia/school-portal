<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Fee Structure</title>
</head>
<body style="border: double black 3px; ">
<table class="table table-stripped">
    <tbody>
    <tr class="text-center">
        <td>
            PRIMARY'S SCHOOL FEE STRUCTURE<br>

        </td>
    </tr>
    <tr class="text-center">
        <td>
            <img src="uploads/logo_1.png" alt="KILIMO" height="100px" width="100px"><br>

        </td>
    </tr>
    <tr class="text-center">
        <td>
            These fees apply only to the current year attendance. Families should expect fees to grow with normal inflation.<br>
        </td>
    </tr>

    </tbody>
</table>

<form method="post" action='{{url("/update_fee_structure")}}'>
    {{csrf_field()}}
    <table class="table table-bordered">
        <thead>
        <th style="font-weight: bold">
            Class
        </th>
        <th style="font-weight: bold">
            Each Term
        </th>
        </thead>
        <tbody>
        <tr>
            <td>

            </td>
            <td>

            </td>
        </tr>

        <tr>
            <td>
                Class 1
            </td>
            <td>
                {{$secretary->class_1}}
            </td>
        </tr>
        <tr>
            <td>
                Class 2
            </td>
            <td>
                {{$secretary->class_2}}
            </td>
        </tr>
        <tr>
            <td>
                Class 3
            </td>
            <td>
                {{$secretary->class_3}}
            </td>
        </tr>
        <tr>
            <td>
                Class 4
            </td>
            <td>
                {{$secretary->class_4}}
            </td>
        </tr>
        <tr>
            <td>
                Class 5
            </td>
            <td>
                {{$secretary->class_5}}
            </td>
        </tr>
        <tr>
            <td>
                Class 6
            </td>
            <td>
                {{$secretary->class_6}}
            </td>
        </tr>
        <tr>
            <td>
                Class 7
            </td>
            <td>
                {{$secretary->class_7}}
            </td>
        </tr>
        <tr>
            <td>
                Class 8
            </td>
            <td>
                {{$secretary->class_8}}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                {{$secretary->notes}}

            </td>
        </tr>
        <tr>
            <td colspan="2" class="text-center" style="font-weight: bold">
                Other fees
            </td>
        </tr>
        <tr>
            <td>Enrollment</td>
            <td>

                {{$secretary->enrollment}}
            </td>
        </tr>
        <tr>
            <td>Caution</td>
            <td>
                {{$secretary->caution}}
            </td>
        </tr>
        <tr>
            <td>Uniform</td>
            <td>
                Varies, details provided on admission day
            </td>
        </tr>
        <tr>
            <td>Caution</td>
            <td>
                Details provided
                to students seeking to register for the bus services, distance also makes price vary
            </td>
        </tr>
        <tr>

        </tr>
        </tbody>


    </table>
</form>
</body>
</html>