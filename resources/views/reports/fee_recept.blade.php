<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Fee Receipt</title>
</head>
<body style="border: double black 3px; ">
<table class="table table-bordered">
    <thead>
    <th  colspan="3" class="text-center" style="text-transform: capitalize; font-weight: bold">
        {{$fee->bank_name}}
    </th>
    </thead>
    <tbody>
    <tr></tr>
    <tr>
        <td>
            Name:  {{$student->student_name}}
        </td>
        <td>
            A/C REF:
        </td>
        <td>
            {{$fee->acc_ref}}
        </td>
    </tr>
    <tr>
        <td>
            Class:   {{$student->year}}
        </td>
        <td>
            A/C REF:
        </td>
        <td>
            {{$fee->acc_no}}
        </td>
    </tr>
    <tr>
        <td>
            Term: {{$student->term}}

        </td>
        <td>
            Account Name:
        </td>
        <td>
            {{$fee->acc_name}}
        </td>
    </tr>
    <tr>
        <td>
            Payment:
        </td>
        <td>
            Date : {{$fee->created_at}}
        </td>
        <td>
            Amount; {{$fee->amount}}
        </td>
    </tr>
    <tr>
        <td>
            Phone Number
        </td>
        <td>
            {{$fee->phone}}
        </td>
    </tr>
    </tbody>

</table>
</body>
</html>