<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Report Form</title>
</head>
<body style="border: double black 3px; ">
<p class="text-center" style="font-weight: bold">{{$student->student_name}}</p>
<p class="text-center"> Class  {{$item->class}} term   {{$item->term}} report card</p>
<p class="text-center"> <img src="uploads/logo_1.png" alt="KILIMO" height="100px" width="100px"></p>
<table class="table table-stripped" style=" color: black; border-radius: 15%; margin: 12%;">
    <thead>
    <th>
        Subject
    </th>
    <th>
        Score
    </th>
    <th>
        Teacher
    </th>
    </thead>
    <tbody>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>Math</td>
        <td>{{$item->math}}</td>
        <td>{{$item->mteacher}}</td>
    </tr>
    <tr>
        <td>Kiswahili</td>
        <td>{{$item->kisw}}</td>
        <td>{{$item->kteacher}}</td>
    </tr>
    <tr>
        <td>English</td>
        <td>{{$item->eng}}</td>
        <td>{{$item->eteacher}}</td>
    </tr>
    <tr>
        <td>Social Studies/CRE</td>
        <td>{{$item->ss}}</td>
        <td>{{$item->steacher}}</td>
    </tr>
    <tr>
        <td>Science</td>
        <td>{{$item->sci}}</td>
        <td>{{$item->sciteacher}}</td>
    </tr>
    <tr>
        <td></td>
        <td>
            Total Score: {{$item->total}} out of 500
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            ClassTeacher's Comment: {{$item->class_teachers_comment}}
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            HeadTeacher's Comment: {{$item->dPrincipals_comment}}
        </td>
    </tr>
    </tbody>

</table>
<style>
.footer {
position: fixed;
left: 0;
bottom: 0;
width: 100%;
background-color: white;
color: black;
text-align: center;
}
</style>
<div class="footer text-center"><p>Providing quality <span style="font-weight: bold; color: limegreen; font-style: italic">education</span></p></div>
</body>
</html>