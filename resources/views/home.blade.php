@extends('layouts.admin')
@section('header')
@endsection
@section('content')
<style type="text/css">
.avatar{
  border-radius: 4%;
  height: auto;
  width: 40%;
}
</style>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                  @if (count($news)>0)
                      @foreach($news->all() as $innews)
                          <img alt="Image Not Available" src="{{$innews->image}}" style="width: 50%; height: auto; border-radius: 4%" class="avatar img-thumbnail">
                          <h5>Event Date: {{$innews->app_date}}</h5>
                          <p>Description:<br> {{$innews->description}}<p>
                          <hr>
                              <ul class="nav nav-pills">

                                  <li><a href="#"><span class="fa fa-eye"> VIEW  </span> </a></li>
                                      <li><a href='{{url("/edit/{$innews->id}")}}'><span class="fa fa-edit"> EDIT </span> </a></li>
                                      <li>  <a href='{{url("/delete/{$innews->id}")}}'><span class="fa fa-trash"> DELETE </span> </a></li>
                              </ul>
                          <hr>
                      @endforeach
                  @else
                      <h3>No News Yet</h3>
                  @endif

@endsection
