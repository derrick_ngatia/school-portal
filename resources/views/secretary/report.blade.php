@extends('layouts.secretary')
@section('content')
    <p class="text-center" style="font-weight: bold;">Generate Report form for {{$student->student_name}}</p>
    <form method="post" action='{{url("/generate/{$student->id}")}}'>
        {{csrf_field()}}
        <div class="form-group">
            <label for="class">Class</label>
            <select class="form-control" name="class">
                @for ($x = 1; $x<=$student->year; $x++)
                    <option value="{{$x}}">{{$x}}</option>
                    @endfor

            </select>


        </div>
        <div class="form-group">
            <label for="term">Term</label>
            <select class="form-control" name="term">
                @for ($x = 1; $x<=3; $x++)
                    <option value="{{$x}}">{{$x}}</option>
                @endfor

            </select>
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-lg" type="submit"><i class="fa fa-file-pdf-o" aria-hidden="true">Generate</i></button>
        </div>
    </form>
    @endsection