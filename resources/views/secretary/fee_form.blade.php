@extends('layouts.secretary')
@section('content')
    <div class="col-md-7">
        <form  action='{{url("/record_fee_payment")}}' method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="acc_ref">A/C REF:</label>
                <div class="form-group{{ $errors->has('acc_ref') ? ' has-error' : '' }}">


                    <div class="col-md-12">
                        <input type="text" value="{{ old('acc_ref') }}" name="acc_ref" class="form-control" required autofocus>
                        @if ($errors->has('acc_ref'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('acc_ref') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
            </div>
            <select class="form-control" name="bank_name">
                <option value="kcb">Kenya Commercial Bank</option>
                <option value="equity">Equity Bank</option>
                <option value="barclays">Barclays Bank</option>
                <option value="cop">Co-operative Bank</option>
            </select>
            <div class="form-group">
                <label for="admission">Enter Student Admission</label>
                <div class="form-group{{ $errors->has('admission') ? ' has-error' : '' }}">


                    <div class="col-md-12">
                        <input type="number" value="{{ old('admission') }}" name="admission" id="admission" class="form-control" required autofocus>
                        @if ($errors->has('admission'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('admission') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="acc_name">Account Name</label>

                <div class="form-group{{ $errors->has('acc_name') ? ' has-error' : '' }}">


                    <div class="col-md-12">
                        <input type="text" value="{{ old('acc_name') }}" name="acc_name" class="form-control" required autofocus>
                        @if ($errors->has('acc_name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('acc_name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="acc_no">Account No</label>
                <div class="form-group{{ $errors->has('acc_no') ? ' has-error' : '' }}">


                    <div class="col-md-12">
                        <input type="number" value="{{ old('acc_no') }}" name="acc_no" class="form-control" required  autofocus>
                        @if ($errors->has('acc_no'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('acc_no') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="phone">Mobile No</label>
                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">


                    <div class="col-md-12">
                        <input type="text" value="{{ old('phone') }}" name="phone" class="form-control" required autofocus>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="amount">Amount</label>
                <input type="number" value="{{ old('amount') }}" name="amount" class="form-control" required autofocus>
                <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">


                    <div class="col-md-12">
                        <input type="text" value="{{ old('amount') }}" name="phone" class="form-control" required autofocus>
                        @if ($errors->has('amount'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
               <button class="btn btn-success" type="submit">Submit</button>
            </div>

        </form>
    </div>

    @endsection