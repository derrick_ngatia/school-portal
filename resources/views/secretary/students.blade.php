@extends('layouts.secretary')
@section('content')
    <form action='{{url("/search_report_form")}}' method="post">
        {{csrf_field()}}
        <div class="input-group col-md-5 -align-center">
            <input type="text" name="search" placeholder="search here" class="form-control"/>
            <span class="input-group-btn">
                <button class="btn btn-default">
                    Go!
                </button>
            </span>
        </div>
    </form>
    <div class="col-md-9">
        @if(count($students)>0)
            <table class="table table-stripped">
                <thead>
                <th>
                    Admission
                </th>
                <th>
                    Name
                </th>
                <th>
                    Current Class
                </th>
                <th>
                    Current Term
                </th>
                </thead>
                @foreach($students as $student)
                 <tbody>
                 <tr>
                     <td>{{$student->admission}}
                     </td>
                     <td>{{$student->student_name}}
                     </td>
                     <td>{{$student->year}}
                     </td>
                     <td>{{$student->term}}
                     </td>
                     <td><a href='{{url("/print_report_form/{$student->id}")}}' class="btn btn-default btn-sm">Report Forms</a>
                     </td>
                 </tr>
                 </tbody>
                @endforeach
                @else
            </table>

        @endif

    </div>
@endsection