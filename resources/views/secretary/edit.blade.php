@extends('layouts.secretary')
@section('content')
    <table class="table table-stripped">
        <thead>
        <th class="text-center">
            PRIMARY'S SCHOOL FEE STRUCTURE
        </th>
        </thead>
        <thead>
        <th class="text-center">
            <img src="uploads/logo_1.png" alt="KILIMO" height="100px" width="100px">
        </th>
        </thead>
        <thead>
        <th class="text-center" style="color: limegreen">
            These fees apply only to the current year attendance. Families should expect fees to grow with normal inflation.
        </th>
        </thead>
    </table>

    <form method="post" action='{{url("/update_fee_structure")}}'>
        {{csrf_field()}}
        <table class="table table-bordered">
            <thead>
            <th style="font-weight: bold">
                Class
            </th>
            <th style="font-weight: bold">
                Each Term
            </th>
            </thead>
            <tbody>
            <tr>
                <td>
                    Class 1
                </td>
                <td>
                    <div class="form-group">
                        <input type="number" name="class_1"  value="{{$secretary->class_1}}" placeholder="amount" class="form-control" required >
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Class 2
                </td>
                <td>
                    <div class="form-group">
                        <input type="number" name="class_2" value="{{$secretary->class_2}}"  placeholder="amount" class="form-control" required >
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Class 3
                </td>
                <td>
                    <div class="form-group">
                        <input type="number" value="{{$secretary->class_3}}" name="class_3"  placeholder="amount" class="form-control" required >
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Class 4
                </td>
                <td>
                    <div class="form-group">
                        <input type="number" value="{{$secretary->class_4}}" name="class_4"  placeholder="amount" class="form-control" required >
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Class 5
                </td>
                <td>
                    <div class="form-group">
                        <input type="number" value="{{$secretary->class_5}}" name="class_5"  placeholder="amount" class="form-control" required >
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Class 6
                </td>
                <td>
                    <div class="form-group">
                        <input type="number" value="{{$secretary->class_6}}" name="class_6"  placeholder="amount" class="form-control" required >
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Class 7
                </td>
                <td>
                    <div class="form-group">
                        <input type="number" value="{{$secretary->class_7}}" name="class_7"  placeholder="amount" class="form-control" required >
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Class 8
                </td>
                <td>
                    <div class="form-group">
                        <input type="number" value="{{$secretary->class_8}}" name="class_8"  placeholder="amount" class="form-control" required >
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <textarea name="notes"  rows="4" cols="4"  class="form-control" placeholder="Important things guardians/parents should note">{{$secretary->notes}}</textarea>

                </td>
            </tr>
            <tr>
                <td colspan="2" class="text-center" style="font-weight: bold">
                    Other fees
                </td>
            </tr>
            <tr>
                <td>Enrollment</td>
                <td>
                    <div class="form-group">
                        <input type="number" value="{{$secretary->enrollment}}" name="enrollment"  placeholder="amount" class="form-control" required >
                    </div>
                </td>
            </tr>
            <tr>
                <td>Caution</td>
                <td>
                    <div class="form-group">
                        <input type="number" name="caution" value="{{$secretary->caution}}"  placeholder="amount" class="form-control" required >
                    </div>
                </td>
            </tr>
            <tr>
                <td>Uniform</td>
                <td>
                    Varies, details provided on admission day
                </td>
            </tr>
            <tr>
                <td>Caution</td>
                <td>
                    Details provided
                    to students seeking to register for the bus services, distance also makes price vary
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button class="btn btn-primary" type="submit">Update Structure</button>
                </td>
            </tr>
            <tr>

            </tr>
            </tbody>


        </table>
    </form>
    <br>
    <br>
    <br>

@endsection
