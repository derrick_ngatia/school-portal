@extends('layouts.help')

@section('content')
<div class="container">
    <div class="row">
        <h3 class="text-center">Primary School Platform Help Center</h3>
        <div class="col-md-8 col-md-offset-3">
            <div class="panel panel-default" style="margin-top: 12%; background-color: darkgrey; color: white;">
                <div class="panel-heading">Experiencing a problem with our system. please give a brief description of the problem you encountered</div>

                <div class="panel-body">
                    <form action='{{url("/submit")}}' method="post" class="form-horizontal">
                        {{csrf_field()}}
                        <div class="form-group{{ $errors->has('help') ? ' has-error' : '' }}">
                            <label for="help" class="col-md-4 control-label">Description</label>

                            <div class="col-md-6">
                                <textarea id="help" name="help" maxlength="250" minlength="100" placeholder="enter a brief description" rows="5" cols="5" class="form-control" required></textarea>
                                @if ($errors->has('help'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('help') }}</strong>
                                  </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="category" class="col-md-4 control-label">Category</label>

                            <div class="col-md-6">
                                <div class="radio">
                                    <label><input type="radio" value="Bugs" name="category" checked>Bugs</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" value="Network Connectivity" name="category">Network Connectivity</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" value="Other" name="category">Other</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-6">
                                <button class="btn btn-success text-center" type="submit" >Submit</button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
