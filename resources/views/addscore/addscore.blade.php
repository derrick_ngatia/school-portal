@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add Kids Score</div>

                <div class="panel-body">
                      <form class="form-horizontal" action="{{url('/addscore')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="students" class="col-md-4 control-label">Students</label>

                            <div class="col-md-6">
                                <select id="students" class="form-control" name="students" required autofocus>
                                  @if (count($students)>0)
                                  @foreach($students->all() as $student)
                                  <option value="{{$student->id}}">{{$student->student_name}}  ID NO: {{$student->id}}</option>
                                  @endforeach
                                      @else
                                      <h3>No students Available</h3>
                                  @endif
                                </select>
                            </div>
                          </div>
                          <div class="form-group">
                              <label for="exam_name" class="col-md-4 control-label">Exam Name</label>

                              <div class="col-md-6">
                                  <select id="exam_name" class="form-control" name="exam_name" required autofocus>
                                    <option value="Opener Exam">Opener Exam</option>
                                    <option value="MidTerm Exam">MidTerm Exam</option>
                                      <option value="EndTerm Exam">EndTerm Exam</option>
                                        <option value="Mock Exam">Mock Exam</option>
                                  </select>
                              </div>
                            </div>
                            <div class="form-group">
                                <label for="year" class="col-md-4 control-label">Year</label>

                                <div class="col-md-6">
                                    <select id="year" class="form-control" name="year" required autofocus>
                                      <option value="1">1</option>
                                      <option value="2">2</option>
                                        <option value="3">3</option>
                                          <option value="4">4</option>
                                    </select>
                                </div>
                              </div>
                              <div class="form-group">
                                  <label for="term" class="col-md-4 control-label">term</label>

                                  <div class="col-md-6">
                                      <select id="term" class="form-control" name="term" required autofocus>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                          <option value="3">3</option>
                                      </select>
                                  </div>
                                </div>
                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                      <label for="score" class="col-md-4 control-label">Score</label>

                      <div class="col-md-6">
                          <input id="score" type="number" class="form-control" value="{{csrf_token()}}" name="score" required autofocus>

                          @if ($errors->has('score'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('score') }}</strong>
                              </span>
                          @endif
                      </div>
              </div>
              <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                  <label for="position" class="col-md-4 control-label">Position</label>

                  <div class="col-md-6">
                      <input id="position" type="number" class="form-control" value="{{csrf_token()}}" name="position" required autofocus>

                      @if ($errors->has('position'))
                          <span class="help-block">
                              <strong>{{ $errors->first('position') }}</strong>
                          </span>
                      @endif
                  </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            addscore
                        </button>
                    </div>
                </div>
                      </form>
    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
