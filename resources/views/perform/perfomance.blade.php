@extends('layouts.admin')
@section('header')
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          @if(count($errors)>0)
          @foreach($errors->all() as $error)
          <div class="alert alert-danger">
            {{$error}}
          </div>
          @endforeach
          @endif
          @if(session('response'))
          <div class="alert alert-success">
            {{session('response')}}
          </div>
          @endif
            <div class="panel panel-default">
                <div class="panel-heading">Post News</div>

                <div class="panel-body">
                  <form class="form-horizontal" method="POST" action="{{url('/news')}}" enctype="multipart/form-data">
                      {{ csrf_field() }}

                      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          <label for="story" class="col-md-4 control-label">Story</label>

                          <div class="col-md-6">
                              <textarea rows="4" cols="50" id="story" class="form-control" placeholder="Enter your story here" name="story" required autofocus></textarea>

                              @if ($errors->has('email'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>
                              @endif
                          </div>
                            <label for="event_Date" class="col-md-4 control-label">Event_Date</label>
                          <div class="col-md-6">
                              <input type="date" id="event_Date" name="date" class="form-control" required autofocus></textarea>

                              @if ($errors->has('event_Date'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('event_Date') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                          <label for="file" class="col-md-4 control-label">Image</label>

                          <div class="col-md-6">
                              <input id="image" type="file" class="form-control" name="image" required>

                              @if ($errors->has('file'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('password') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-md-8 col-md-offset-4">
                              <button type="submit" onclick=""class="btn btn-primary">
                                  Post News
                              </button>
                          </div>
                      </div>
                  </form>
                </div>
        </div>
    </div>
</div>
@endsection
