@extends('layouts.parenthome')
@section('content')
    <div class="col-md-10">
        <h5 class="text-center" style="font-weight: bold">Cases of Indiscipline</h5>
        @if(count($cases)>0)
            @foreach($cases as $case)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h6 class="text-center">{{$case->offense}}</h6>
                    </div>
                    <div class="panel-body">
                        <p>Date Commited: {{$case->created_at}}</p>
                        <p>Disciplinary Action Taken: <span style="color: red">{{$case->disciplinary_action}}</span> </p>
                        <p>Warning Level: <span style="color: red">{{$case->warning}}</span> </p>
                        <p style="color: darkblue">@if($case->cleared==1)
                                Case pending....
                            @else
                                Cleared
                            @endif
                        </p>

                    </div>

                </div>
            @endforeach
        @else
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span style="color: darkgreen">No records of cases of indiscipline</span>
                </div>

            </div>
        @endif

    </div>
@endsection