@extends('layouts.parenthome')
@section('content')
    <table class="table table-stripped">
        <thead>
        <th class="text-center">
            PRIMARY'S SCHOOL FEE STRUCTURE
        </th>
        </thead>
        <thead>
        <th class="text-center">
            <img src="uploads/logo_1.png" alt="KILIMO" height="100px" width="100px">
        </th>
        </thead>
        <thead>
        <th class="text-center" style="color: limegreen">
            These fees apply only to the current year attendance. Families should expect fees to grow with normal inflation.
        </th>
        </thead>
    </table>

    <form method="post" action='{{url("/update_fee_structure")}}'>
        {{csrf_field()}}
        <table class="table table-bordered">
            <thead>
            <th style="font-weight: bold">
                Class
            </th>
            <th style="font-weight: bold">
                Each Term
            </th>
            </thead>
            <tbody>
            <tr>
                <td>
                    Class 1
                </td>
                <td>
                    {{$secretary->class_1}}
                </td>
            </tr>
            <tr>
                <td>
                    Class 2
                </td>
                <td>
                    {{$secretary->class_2}}
                </td>
            </tr>
            <tr>
                <td>
                    Class 3
                </td>
                <td>
                    {{$secretary->class_3}}
                </td>
            </tr>
            <tr>
                <td>
                    Class 4
                </td>
                <td>
                    {{$secretary->class_4}}
                </td>
            </tr>
            <tr>
                <td>
                    Class 5
                </td>
                <td>
                    {{$secretary->class_5}}
                </td>
            </tr>
            <tr>
                <td>
                    Class 6
                </td>
                <td>
                    {{$secretary->class_6}}
                </td>
            </tr>
            <tr>
                <td>
                    Class 7
                </td>
                <td>
                    {{$secretary->class_7}}
                </td>
            </tr>
            <tr>
                <td>
                    Class 8
                </td>
                <td>
                    {{$secretary->class_8}}
                </td>
            </tr>
            <tr>
                <td colspan="2">
                   {{$secretary->notes}}

                </td>
            </tr>
            <tr>
                <td colspan="2" class="text-center" style="font-weight: bold">
                    Other fees
                </td>
            </tr>
            <tr>
                <td>Enrollment</td>
                <td>

                       {{$secretary->enrollment}}
                </td>
            </tr>
            <tr>
                <td>Caution</td>
                <td>
                       {{$secretary->caution}}
                </td>
            </tr>
            <tr>
                <td>Uniform</td>
                <td>
                    Varies, details provided on admission day
                </td>
            </tr>
            <tr>
                <td>Caution</td>
                <td>
                    Details provided
                    to students seeking to register for the bus services, distance also makes price vary
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <a href='{{url("/print_fee_structure_parent")}}' class="btn btn-primary" type="submit"><i class="fa fa-print" aria-hidden="true">Print Fee Structure</i></a>
                </td>
            </tr>
            <tr>

            </tr>
            </tbody>


        </table>
    </form>
    <br>
    <br>
    <br>

@endsection
