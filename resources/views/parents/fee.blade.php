@extends('layouts.parenthome')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="text-center">My Child's Information</h3>
        </div>




        <div class="panel-body">
            @foreach($students as $student)
                <table class="table table-stripped">
                    <thead>
                    <th>
                        Admission Number
                    </th>
                    <th>
                        Profile
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                        Current Class
                    </th>
                    <th>
                        Term
                    </th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$student->admission}}</td>
                        <td><img src='{{asset("storage/profiles/$student->imageurl")}}' alt="No profile" height="50px" width="50px"></td>
                        <td>{{$student->student_name}}</td>
                        <td>{{$student->year}}</td>
                        <td>{{$student->term}}</td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="6">
                            <a href='{{url("/fee/{$student->id}")}}' class="btn btn-success">Fee paid</a>
                        </td>
                    </tr>
                    </tbody>

                </table>
            @endforeach


        </div>
    </div>
@endsection