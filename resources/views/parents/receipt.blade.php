@extends('layouts.parenthome')
@section('content')
    <div class="panel panel-default">
        @if(count($fees)>0)
            @foreach($fees as $fee)
                <table class="table table-bordered">
                    <thead>
                        <th  colspan="3" class="text-center" style="text-transform: capitalize; font-weight: bold">
                            {{$fee->bank_name}}
                        </th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                          Name:  {{$student->student_name}}
                        </td>
                        <td>
                            A/C REF:
                        </td>
                        <td>
                            {{$fee->acc_ref}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                         Class:   {{$student->year}}
                        </td>
                        <td>
                            A/C REF:
                        </td>
                        <td>
                            {{$fee->acc_no}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Term: {{$student->term}}

                        </td>
                        <td>
                          Account Name:
                        </td>
                        <td>
                            {{$fee->acc_name}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Payment:
                        </td>
                        <td>
                            Date : {{$fee->created_at}}
                        </td>
                        <td>
                            Amount; {{$fee->amount}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Phone Number
                        </td>
                        <td>
                            {{$fee->phone}}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center">
                            <a href='{{url("/print_fee_receipt/{$fee->id}")}}' class="btn btn-default"><i class="fa fa-print" aria-hidden="true">Print Fee Receipt</i></a>

                        </td>
                    </tr>
                    </tbody>

                </table>

                @endforeach
        @else
            <div class="panel-heading">
                No records of fee payment for this student.
            </div>
    </div>

    @endif
    @endsection