<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="uploads/logo_1.png" />
    <title> PORTAL</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body background="uploads/backie.jpg">
<div id="app">
    <div>
        @include('flash::message')
        @yield('content')
    </div>
<div>

</div>
</div>
</div>
<style>
    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: white;
        color: black;
        text-align: center;
    }
</style>

<div class="footer">
    <p>&#9728; Offering Quality Education</p>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
