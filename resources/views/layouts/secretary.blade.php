<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="uploads/logo_1.png" />
    <title> PORTAL</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body background="uploads/backie.jpg">
<style>
    ul li{
        padding-bottom: 20px;
        font-width:bold;
    }
    a{
        font-weight: bold;
        color: white;
    }

</style>
<div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="background-color: limegreen">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href='{{"/secretary"}}'>
                        <i class="fa fa-home" aria-hidden="true">
                            PRIMARY SCHOOL PORTAL

                        </i>

                    </a>

                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li><a href='{{"/secretary"}}'><i class="fa fa-home" aria-hidden="true">Home</i></a>
                        </li>
                        <li><a href="{{url('/aboutus')}}"><i class="fa fa-address-book" aria-hidden="true">About us</i></a>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            {{-- <li><a href="{{ route('register') }}">Register</a></li>--}}
                        @else
                            <li><a><unread :user_id="{{Auth::user()->id}}"></unread> Messages</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"

                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            &#x21e8; Logout

                                        </a>

                                        <a href="/help">
                                            <i class="far fa-question-circle"> Help</i>
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-3" style="border-radius: 10%; background-color: burlywood">
                <ul class="nav nav-pills nav-stacked navbar-fixed-left ">
                    <li><a href='{{url("/fee_payment")}}'>Record Fee Payment</a></li>
                    <li><a href="{{url("/generate_report_forms")}}">Report Forms</a></li>
                    <li><a href='{{url("/fee_structure")}}'>Post Fee Structure</a></li>
                    <li><a href='{{url("/edit_fee_structure")}}'>Edit Fee Structure</a></li>
                    <li><a href="#">Queries and Requests</a></li>
                    <li><a href='{{url("/news")}}'>Schools News</a></li>
                </ul>

            </div>
            <div class="col-md-9">
                @include('flash::message')
                @yield('content')
            </div>
        </div>
    </div>

</div>

</body>
<style>
    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: white;
        color: black;
        text-align: center;
    }
</style>

<div class="footer">
    <p>&#9728; Offering Quality Education</p>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</html>