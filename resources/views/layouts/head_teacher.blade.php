<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="uploads/logo_1.png" />
    <title> PORTAL</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body background="uploads/backie.jpg">
<div id="app">
    <nav class="navbar navbar-default navbar-static-top" style="background-color: wheat; color: white;">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href='{{"/parentshome"}}'>
                   <span> HEAD TEACHER</span>
                </a>

            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href='{{"/headteacher"}}' class="fa fa-home">Home</a>
                    </li>
                    <li><a href="{{url('/aboutus')}}"><i class="fa fa-address-book" aria-hidden="true">About us</i></a>
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        {{-- <li><a href="{{ route('register') }}">Register</a></li>--}}
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"

                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        &#x21e8; Logout

                                    </a>

                                    <a href="/help">
                                        <i class="far fa-question-circle"> Help</i>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>


</div>
<style>
    ul li{
        padding-bottom: 20px;
        font-width:bold;
    }

</style>

<div class="container" >
    <style>
        a{
            color: white;
            font-weight: bold;
        }
        a:hover{
            background-color: fuchsia;
        }
        .col-md-3{
            border-radius: 10%;
        }
    </style>
    <div class="row">
        <div class="col-md-3" style="background-color: #31b0d5; color: white;">
            <ul class="nav nav-pills nav-stacked navbar-fixed-left ">
                <li><a href='{{url("/headteacher")}}'>Student Performance/Comments</a></li>
                <li><a href="{{url("/disciplinary_pending_cases")}}">Disciplinary Cases</a></li>
                <li><a href='{{url("/register_case_disciplinary")}}'>Register a Disciplinary Case</a></li>
                <li><a href='{{url("/disciplinary_pending_cases")}}'>Clear a Disciplinary Case</a></li>
                <li><a href='{{url("/school_disciplinary_actions")}}'>Punishment Categories</a></li>
                <li><a href='{{url("/schools_events")}}'>Upcoming School Events</a></li>
            </ul>

        </div>
        <div class="col-md-9">
            @include('flash::message')
            @yield('content')
        </div>
    </div>
</div>
<style>
    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: white;
        color: black;
        text-align: center;
    }
</style>

<div class="footer">
    <p>&#9728; Offering Quality Education</p>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>