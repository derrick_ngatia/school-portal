@extends('layouts.classteacher')
@section('content')
    <table class="table table-bordered">
        <thead>
        <th colspan="3" class="text-center">Mark Sheet</th>
        </thead>
        <thead>
        <th>{{$student->student_name}}</th>
        <th>Class:{{$student->year}}</th>
        <th>Term:{{$student->term}}</th>
        </thead>
        <thead>
        <th>Subject</th>
        <th>Mark</th>
        <th>Techer Name(initials)</th>
        </thead>
        <tbody>
        <form class="form-group" action='{{url("/update_score/{$student->id}/{$score_id}")}}' method="post" >
            {{ csrf_field() }}
            <tr>
                <td>Mathematics</td>
                <td><input type="number" value="{{$score->math}}" id="math" name="math" required></td>
                @if ($errors->has('math'))
                    <span class="help-block">
                       <strong>{{ $errors->first('math') }}</strong>
                   </span>
                @endif
                <td><input type="text" value="{{$score->mteacher}}" id="mteacher" name="mteacher" required></td>
            </tr>
            <tr>
                <td>English</td>
                <td><input type="number" id="eng"  value="{{$score->eng}}"  name="eng" required></td>
                @if ($errors->has('eng'))
                    <span class="help-block">
                       <strong>{{ $errors->first('eng') }}</strong>
                   </span>
                @endif
                <td><input type="text" id="eteacher" value="{{$score->eteacher}}" name="eteacher" required></td>
            </tr>
            <tr>
                <td>Kiswahili</td>
                <td><input type="number" id="kisw" value="{{$score->kisw}}" name="kisw" required></td>
                @if ($errors->has('kisw'))
                    <span class="help-block">
                       <strong>{{ $errors->first('kisw') }}</strong>
                   </span>
                @endif
                <td><input type="text" id="kteacher" value="{{$score->kteacher}}" name="kteacher" required></td>
            </tr>
            <tr>
                <td>Social Studies/CRE</td>
                <td><input type="number" value="{{$score->ss}}" id="ss" name="ss" required></td>
                @if ($errors->has('ss'))
                    <span class="help-block">
                       <strong>{{ $errors->first('ss') }}</strong>
                   </span>
                @endif
                <td><input type="text" id="steacher" value="{{$score->steacher}}" name="steacher" required></td>
            </tr>
            <tr>
                <td>Science</td>
                <td><input type="number" value="{{$score->sci}}" id="sci" name="sci" required></td>
                @if ($errors->has('sci'))
                    <span class="help-block">
                       <strong>{{ $errors->first('sci') }}</strong>
                   </span>
                @endif
                <td><input type="text" id="sciteacher" value="{{$score->sciteacher}}" name="sciteacher" required></td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="form-group">
                        <textarea  class="form-control" cols="6" rows="5" id="comment" name="comment" placeholder="Add a comment to student Score" required>
                            {{$score->class_teachers_comment}}
                        </textarea>
                    </div>

                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="3"><button type="submit" class="btn btn-success btn-md">update</button></td>
            </tr>
        </form>

        </tbody>
    </table>
@endsection