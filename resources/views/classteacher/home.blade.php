@extends('layouts.classteacher')
@section('content')
    @if (count($news)>0)
        @foreach($news->all() as $innews)
            <img alt="Image Not Available" src="{{$innews->image}}" style="width: 50%; height: auto; border-radius: 4%" class="avatar img-thumbnail">
            <h5>Event Date: {{$innews->app_date}}</h5>
            <p>Description:<br> {{$innews->description}}<p>
            <hr>
            <hr>
        @endforeach
    @else
        <h3 class="text-center">No News Yet</h3>
    @endif
    @endsection