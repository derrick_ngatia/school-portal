@extends('layouts.classteacher')
@section('content')
    <table class="table table-bordered">
        <thead>
        <th colspan="3" class="text-center">Mark Sheet</th>
        </thead>
        <thead>
        <th>{{$student->student_name}}</th>
        <th>Class:{{$student->year}}</th>
        <th>Term:{{$student->term}}</th>
        </thead>
        <thead>
        <th>Subject</th>
        <th>Mark</th>
        <th>Techer Name(initials)</th>
        </thead>
      <tbody>
      <form class="form-group" action='{{url("/store_score/{$student->id}")}}' method="post" >
          {{ csrf_field() }}
          <tr>
              <td>Mathematics</td>
              <td>
                  <div class="form-group{{ $errors->has('math') ? ' has-error' : '' }}">


                      <div class="col-md-12">
                          <input type="number" value="{{ old('math') }}" id="math" name="math" required autofocus>
                          @if ($errors->has('math'))
                              <span class="help-block">
                                        <strong>{{ $errors->first('math') }}</strong>
                                    </span>
                          @endif
                      </div>
                  </div>
              </td>
              <td><input type="text" value="{{ old('mteacher') }}" id="mteacher" name="mteacher" required></td>
          </tr>
          <tr>
              <td>English</td>
              <td>

                  <div class="form-group{{ $errors->has('eng') ? ' has-error' : '' }}">


                      <div class="col-md-12">
                          <input type="number" value="{{ old('eng') }}" id="eng" name="eng" required autofocus>
                          @if ($errors->has('eng'))
                              <span class="help-block">
                                        <strong>{{ $errors->first('eng') }}</strong>
                                    </span>
                          @endif
                      </div>
                  </div>

              </td>
              <td><input type="text" value="{{ old('eteacher') }}" id="eteacher" name="eteacher" required></td>
          </tr>
          <tr>
              <td>Kiswahili</td>
              <td>
                  <div class="form-group{{ $errors->has('kisw') ? ' has-error' : '' }}">


                      <div class="col-md-12">
                          <input type="number" value="{{ old('kisw') }}" id="kisw" name="kisw" required autofocus>
                          @if ($errors->has('kisw'))
                              <span class="help-block">
                                        <strong>{{ $errors->first('kisw') }}</strong>
                                    </span>
                          @endif
                      </div>
                  </div>
              </td>
              <td><input type="text" value="{{ old('kteacher') }}" id="kteacher" name="kteacher" required></td>
          </tr>
          <tr>
              <td>Social Studies/CRE</td>
              <td>
                  <div class="form-group{{ $errors->has('ss') ? ' has-error' : '' }}">


                      <div class="col-md-12">
                          <input type="number" value="{{ old('ss') }}" id="ss" name="ss" required autofocus>
                          @if ($errors->has('ss'))
                              <span class="help-block">
                                        <strong>{{ $errors->first('ss') }}</strong>
                                    </span>
                          @endif
                      </div>
                  </div>
              </td>
              <td><input type="text" id="steacher" value="{{ old('steacher') }}" name="steacher" required></td>
          </tr>
          <tr>
              <td>Science</td>
              <td>
                  <div class="form-group{{ $errors->has('sci') ? ' has-error' : '' }}">


                      <div class="col-md-12">
                          <input type="number" id="sci" value="{{ old('sci') }}" name="sci" required autofocus>
                          @if ($errors->has('sci'))
                              <span class="help-block">
                                        <strong>{{ $errors->first('sci') }}</strong>
                                    </span>
                          @endif
                      </div>
                  </div>
              </td>
              <td><input type="text" value="{{ old('sciteacher') }}" id="sciteacher" name="sciteacher" required></td>
          </tr>
          <tr>
              <td colspan="3">
                  <div class="form-group">
                      <textarea  class="form-control" value="{{ old('comment') }}" cols="6" rows="5" id="comment" name="comment" placeholder="Add a comment to student Score" required></textarea>
                  </div>

              </td>
          </tr>
          <tr>
             <td class="text-center" colspan="3"><button type="submit" class="btn btn-success btn-md">Submit</button></td>
          </tr>
      </form>

      </tbody>
    </table>
@endsection