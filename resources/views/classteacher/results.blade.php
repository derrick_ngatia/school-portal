@extends('layouts.classteacher')
@section('content')
    <table class="table table-stripped">
        <thead>
        <th>Photo</th>
        <th>Name</th>
        <th>Class</th>
        <th>Term</th>

        </thead>
        <tbody>
        @foreach($students as $student)
            <tr>
                <td><img src='{{asset("storage/profiles/$student->imageurl")}}' alt="No profile" height="50px" width="50px"></td>
                <td>{{$student->student_name}}</td>
                <td>{{$student->year}}</td>
                <td>{{$student->term}}</td>
                <td><a href='{{url("/view_marks/{$student->id}")}}' class="btn btn-success btn-sm">View Student Marks</a> </td>
            </tr>
        @endforeach
        </tbody>

    </table>
@endsection