@extends('layouts.classteacher')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="text-center">{{$student->admission}}   {{$student->student_name}}</h4>
        <h5 class="text-center">Class: {{$student->year}} Term: {{$student->term}}</h5>

    </div>
    @foreach($score as $item)
<div class="panel-body">
    <table class="table table-stripped">
       Results last updated on {{$item->updated_at}}
        <thead>
        <th>
          Class  {{$item->class}}
        </th>
        <th>
            Term  {{$item->term}}
        </th>
        </thead>
        <thead style="color: brown">
        <th>
            Subject
        </th>
        <th>
            Score
        </th>
        <th>
            Teacher
        </th>
        </thead>
        <tbody>
        <tr>
            <td>Math</td>
            <td>{{$item->math}}</td>
            <td>{{$item->mteacher}}</td>
        </tr>
        <tr>
            <td>Kiswahili</td>
            <td>{{$item->kisw}}</td>
            <td>{{$item->kteacher}}</td>
        </tr>
        <tr>
            <td>English</td>
            <td>{{$item->eng}}</td>
            <td>{{$item->eteacher}}</td>
        </tr>
        <tr>
            <td>Social Studies/CRE</td>
            <td>{{$item->ss}}</td>
            <td>{{$item->steacher}}</td>
        </tr>
        <tr>
            <td>Science</td>
            <td>{{$item->sci}}</td>
            <td>{{$item->sciteacher}}</td>
        </tr>
        <tr>
            <td colspan="2">
               Total Score: {{$item->total}} out of 500
            </td>
            <td>
                <a href='{{url("/editscore/{$student->id}/{$item->id}")}}' class="btn btn-success btn-sm">Edit Score</a>
            </td>
        </tr>
        <tr>
        </tr>
        </tbody>
    </table>

</div>
        @endforeach
</div>
@endsection