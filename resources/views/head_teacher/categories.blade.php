@extends('layouts.head_teacher')
@section('content')
    <h3 class="text-center">Discplinary Actions Explained</h3>
    <p style="text-justify: auto; color: grey; font-style: italic">
        Caning is a form of corporal punishment consisting of a number of hits (known as "strokes" or "cuts") with a single cane usually made of rattan, generally applied to the offender's bare or clothed buttocks (see spanking) or hand(s) (on the palm). Caning on the knuckles or shoulders is much less common. Caning can also be applied to the soles of the feet (foot whipping or bastinado). The size and flexibility of the cane and the mode of application, as well as the number of the strokes, vary greatly — from a couple of light strokes with a small cane across the seat of a junior schoolboy's trousers, to 24 very hard, wounding cuts on the bare buttocks with a large, heavy, soaked rattan as a judicial punishment in some Southeast Asian countries.

        The thin cane generally used for corporal punishment is not to be confused with a walking stick, sometimes also called a cane (especially in American English), but which is thicker and much more rigid, and more likely to be made of stronger wood than of cane.
    </p>
    <p style="text-justify: auto; color: grey; font-style: italic">
        Expulsion, permanent exclusion, withdrawing, or kicked out of school[1] refers to the removal/banning of a student from a school system or university for an extensive period of time due to a student persistently violating that institution's rules, or for a single offense of appropriate severity in extreme cases
    </p>
    <p style="text-justify: auto; color: grey; font-style: italic">
        Part of a series on
        Corporal punishment
        By place
        Domestic School Judicial
        By implementation
        Belting Birching Caning Cat o' nine tails Flagellation Foot whipping Knout Paddle Scourge Slippering Spanking Strapping Switch Tawse Riding crop Whip
        By country
        Afghanistan Brunei Iran Malaysia Saudi Arabia Qatar Singapore Taiwan United Arab Emirates
        Court cases
        CFCYL v. Canada Ingraham v. Wright
        Politics
        Campaigns against
        corporal punishment
        vte

        Bastinado demonstration using a cane
        Foot whipping or bastinado is a method of corporal punishment which consists of hitting the bare soles of a person's feet. Unlike most types of flogging, this punishment was meant to be more painful than it was to cause actual injury to the victim. Blows were generally delivered with a light rod, knotted cord, or lash.[1]

        The receiving person is required to be barefoot. The uncovered soles of the feet need to be placed in an exposed position. The beating is typically performed with an object in the type of a cane or switch. The strokes are usually aimed at the arches of the feet and repeated a certain number of times.
    </p>
    <p style="text-justify: auto; color: grey; font-style: italic">
        The hands up punishment is a form of punishment given in schools of the Indian subcontinent, especially in India, Bangladesh, Sri Lanka and Pakistan. In this punishment, one is made to raise his or her hands above their head—a stress position—and remain in this state for a period of time.[1][unreliable source] The recipient of the punishment is not permitted to join their hands above their head. The hands up position becomes painful within ten or fifteen minutes. This is generally done to correct mischievous students in schools.
    </p>
    <p style="text-justify: auto; color: grey; font-style: italic">
        A write out is a punishment used in schools in which a misbehaving student must write something out. It is different from writing lines as, instead of writing something out a number of times, the student might write out a page, chapter or paragraph of their schoolbook, workbook or novel. In the case of non-written homework (spellings), the student might have to write the spellings a number of times.
    </p>
    <p style="text-justify: auto; color: grey; font-style: italic">
        Gating is a type of punishment similar to a detention used typically at educational institutions, especially boarding schools. Precisely what a gating consists of and the rules surrounding it will vary between institutions, but the common element is that someone who has been gated is not permitted to leave the establishment. The word is used as both a noun and a verb.
    </p>
<br>
    <br>
    <br>


@endsection