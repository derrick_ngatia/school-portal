@extends('layouts.head_teacher')
@section('content')
    <div class="col-md-8">
        <form action='{{url("/register_case")}}' method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="admission">Student Admission Number</label>
                <input type="number" name="admission"  value="{{ old('admission') }}" class="form-control" required>

            </div>
            <div class="form-group">
                <label for="offense">Offense</label>
                <textarea  name="offense" value="{{ old('offense') }}" rows="6" class="form-control" required></textarea>

            </div>
            <div class="form-group">
                <label for="disciplinary_action">Discplinary Action Taken</label>
                <textarea name="disciplinary_action" value="{{ old('disciplinary_action') }}" rows="6" class="form-control" require></textarea>

            </div>
            <div class="form-group">
                <label for="warning">Warning</label>
                <select name="warning" class="form-control">
                    <option value="1st Warning">first warning</option>
                    <option value="2nd Warning">second warning</option>
                    <option value="3rd Warning">third warning</option>
                    <option value="Last Warning">last warning</option>
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-success" type="submit">Register Case</button>
            </div>
            <br>
            <br>
            <br>
        </form>
    </div>

    @endsection