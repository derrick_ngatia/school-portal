@extends('layouts.head_teacher')
@section('content')
    <form action='{{url("/search_student_h")}}' method="post">
        {{csrf_field()}}
        <div class="input-group col-md-5 -align-center">
            <input type="text" name="search" placeholder="search here" class="form-control"/>
            <span class="input-group-btn">
                <button class="btn btn-default">
                    Go!
                </button>
            </span>
        </div>
    </form>
    <div class="panel">
<table class="table table-stripped">
    <thead>
    <th>
        Profile
    </th>
    <th>
        Admission
    </th>
    <th>
        Name
    </th>
    <th>
        Class
    </th>
    <th>
        Term
    </th>
    <th>
        View Profile
    </th>
    </thead>
    @if(count($students)>0)
    @foreach($students as $student)
        <tbody>
        <tr>
            <td><img src='{{asset("storage/profiles/$student->imageurl")}}' alt="No profile" height="30px" width="30px"></td>
            <td>
                {{$student->admission}}
            </td>
            <td>
                {{$student->student_name}}
            </td>
            <td>
                {{$student->year}}
            </td>
            <td>
                {{$student->term}}
            </td>
            <td>
                <a href='{{url("/student_profile/{$student->id}")}}' class="btn btn-default btn-sm">View Student Profile</a>
            </td>
        </tr>
        </tbody>
        @endforeach
        @else
       <span class="span4s" style="color: red; font-weight: bold">No results</span>
    @endif

</table>
    </div>
    @endsection