@extends('layouts.head_teacher')
@section('content')
    <h4 class="text-center">Pending Disciplinary Cases</h4>
    <form action='{{url("/search_student_c")}}' method="post">
        {{csrf_field()}}
        <div class="input-group col-md-5 -align-center">
            <input type="text" name="search" placeholder="search here" class="form-control"/>
            <span class="input-group-btn">
                <button class="btn btn-default">
                    Go!
                </button>
            </span>
        </div>
    </form>
    <table class="table table-stripped">
        <thead>
        <th>
            Admission
        </th>
        <th>
            Name
        </th>
        <th>
            Class
        </th>
        <th>
            Term
        </th>
        <th>
            Offense
        </th>
        <th>
            Action Taken
        </th>
        <th>
            Warning
        </th>
        <th>
            Cleared
        </th>

        </thead>
        @foreach($cases as $case)
<tbody>
<tr>
    <td>{{$case->admission}}</td>
    <td>{{$case->student_name}}</td>
    <td>{{$case->class}}</td>
    <td>{{$case->term}}</td>
    <td> on {{$case->created_at}} {{$case->student_name}} did the following.<br><span style="color: fuchsia">{{$case->offense}}</span> </td>
    <td>{{$case->disciplinary_action}}</td>
    <td>{{$case->warning}}</td>
    <td>Not cleared</td>
    <td><a href='{{url("/clear_case/{$case->id}/{$case->c_id}")}}' class=" btn btn-success btn-sm">Clear</a> </td>
</tr>

</tbody>
            @endforeach
    </table>

    @endsection