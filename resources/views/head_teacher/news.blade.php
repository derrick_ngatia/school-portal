@extends('layouts.head_teacher')
@section('content')
@if(count($news)>0)

    @foreach($news as $new)
    <div class="panel panel-default">
        <div class="panel-heading">
        <p class="text-center">posted on {{$new->created_at}}</p>
        </div>
        <div class="panel-body">
            <p class="text-center">            <img alt="Image Not Available" src="{{$new->image}}" style="width: 50%; height: auto; border-radius: 4%" class="avatar img-thumbnail">

            </p>
        <p class="text-center" style="text-justify: auto; font-style: italic; font-weight: bold">{{$new->description}}</p>
            <p class="text-center" style="text-justify: auto; font-style: italic; font-weight: bold">Happening on {{$new->app_date}}</p>

        </div>
    </div>
    @endforeach
    @else
    <div class="panel-heading">
       <p class="text-center"> <span class="text-center" style="color: fuchsia; font-weight: bold ">No News posted Yet</span></p>
    </div>
    @endif
@endsection