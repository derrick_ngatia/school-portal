@extends('layouts.head_teacher')
@section('content')
<div class="row" style="background-color: white">
    <h4 class="text-center">{{$student->student_name}}'s Profile</h4>
<div class="col-md-2">
        <img src='{{asset("storage/profiles/$student->imageurl")}}' alt="No profile" height="100px" width="100px">

    </div>
    <div class="col-md-9"  style="background-color: beige">
        <p class="text-center">Name: <span style="color: #2E0400; font-weight: bold; text-justify: auto">{{$student->student_name}}</span></p>
        <p class="text-center">Class: <span style="color: #2E0400; font-weight: bold;text-justify: auto ">{{$student->year}}</span></p>
        <p class="text-center">Term: <span style="color: #2E0400; font-weight: bold; text-justify: auto">{{$student->term}}</span></p>


    </div>
    <div class="col-md-9">
        @if(count($score)>0)
            <div class="col-md-10" style="background-color: beige" >
                <p><span style="font-weight: bold;color: limegreen; font-style: italic">Current Score: {{$score->total }} out of 500</span></p>
                <table class="table table-stripped">
                    <tbody>
                    <tr>
                        <td>
                            <p><span style="color: red;">Class teacher's Comment: <span style="text-decoration: underline; color: darkslategrey">{{$score->class_teachers_comment}}</span></span></p>
                        </td>


                    </tr>
                    <tr>
                        <td>
                            @if($score->parents_comment==null)
                                <p><span style="color: red;">Parent's Comment  <span style="text-decoration: underline; color: darkslategrey">Not available</span></span></p>
                            @else
                                <p><span style="color: red;">Parent's Comment:  <span style="text-decoration: underline; color: darkslategrey">{{$score->parents_comment}}</span></span></p>

                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            @if($score->dPrincipals_comment==null)
                                <form method="post" action='{{url("/head_teacher_comment/{$student->id}")}}'>
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <textarea class="form-control" id="comment" name="comment" rows="4"placeholder="add marks Comment" cols="4" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success btn-sm" type="submit">Post Comment</button>
                                    </div>
                                </form>
                            @else
                                <p><span style="color: red;">HeadTeacher's Comment:  <span style="text-decoration: underline; color: darkslategrey">{{$score->dPrincipals_comment}}</span></span></p>

                            @endif
                        </td>

                    </tr>
                    </tbody>

                </table>

            </div>
        @endif

    </div>
    <div class="col-md-10">
        <h5 class="text-center" style="font-weight: bold">Cases of Indiscipline</h5>
        @if(count($cases)>0)
            @foreach($cases as $case)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h6 class="text-center">{{$case->offense}}</h6>
                    </div>
                    <div class="panel-body">
                        <p>Date Commited: {{$case->created_at}}</p>
                        <p>Disciplinary Action Taken: <span style="color: red">{{$case->disciplinary_action}}</span> </p>
                        <p>Warning Level: <span style="color: red">{{$case->warning}}</span> </p>
                        <p style="color: darkblue">@if($case->cleared==1)
                                Case pending....
                            @else
                                Cleared
                            @endif
                        </p>

                    </div>

                </div>
            @endforeach
        @else
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span style="color: darkgreen">No records of cases of indiscipline</span>
                </div>

            </div>
        @endif

    </div>

</div>
    @endsection