@extends('layouts.admin')
@section('header')
    <h2>Update class teacher details</h2>
@endsection
@section('content')

    <form class="form-horizontal" method="POST" action='{{url("update-class-teachers/{$teacher->staff_no}")}}'>
    {{ csrf_field() }}


            <div class="col-md-5">
                <label>Current Class:{{$teacher->class}}</label><br>
                <label for="class">New Class</label>
                <div class="form-group{{ $errors->has('class') ? ' has-error' : '' }}">


                    <div class="col-md-12">
                        <input placeholder="Class" id="class" type="number" class="form-control" name="class" value="{{ old('class') }}" required autofocus>

                        @if ($errors->has('class'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('class') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
                <a href='{{url("/delete-class-teacher/{$teacher->staff_no}")}}' class="btn btn-danger" onclick="
                return confirm('Are you sure you want to remove {{$teacher->staff_no}} as a class teacher?')">Remove</a>

            </div>

    </form>
@endsection