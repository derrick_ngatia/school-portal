@extends('layouts.admin')
@section('header')
@endsection
@section('content')
    <form action='{{url("/search_student")}}' method="post">
        {{csrf_field()}}
        <div class="input-group col-md-5 -align-center">
            <input type="text" name="search" placeholder="search by admission No" class="form-control"/>
            <span class="input-group-btn">
                <button class="btn btn-default">
                    Go!
                </button>
            </span>
        </div>
    </form>
    <table class="table table-striped">
      <thead>
      <th>
          Admission Number
      </th>
      <th>
          Photo
      </th>
      <th>
          Name
      </th>
      <th>
          Parent Email
      </th>
      <th>
          Class
      </th>
      <th>
          Term
      </th>
      </thead>
        <tbody>
       @foreach($students as $student)
           <tr>
               <td>{{$student->admission}}</td>
               <td><img src='{{asset("storage/profiles/$student->imageurl")}}' alt="No profile" height="50px" width="50px"></td>
               <td>{{$student->student_name}}</td>
               <td>{{$student->parent_email}}</td>
               <td>{{$student->year}}</td>
               <td>{{$student->term}}</td>
           </tr>
           @endforeach
        </tbody>

    </table>

    @endsection