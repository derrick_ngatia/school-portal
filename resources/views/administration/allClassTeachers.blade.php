@extends('layouts.admin')
@section('header')
@endsection
@section('content')
    <form action='{{url("/search_class_teacher")}}' method="post">
        {{csrf_field()}}
        <div class="input-group col-md-5 -align-center">
            <input type="text" name="search" placeholder="search here" class="form-control"/>
            <span class="input-group-btn">
                <button class="btn btn-default">
                    Go!
                </button>
            </span>
        </div>
    </form>
    <table class=" table table-stripped">
        <thead>
        <th>
            Name
        </th>
        <th>
            Gender
        </th>
        <th>
           Email
        </th>
        <th>
            Class
        </th>
        <th>
            Action
        </th>
        </thead>
        <tbody>
        @foreach($classTeachers as $single)
        <tr>
            <td>{{$single->name}}</td>
            <td>
                @if($single->gender==1)
                    Female
                    @else
                Male
                    @endif
            </td>
            <td>{{$single->email}}</td>
            <td>{{$single->class}}</td>
            <td><a href='{{url("update-class-teacher/{$single->staff_no}")}}' class="btn btn-success">Update</a> </td>
        </tr>
            @endforeach
        </tbody>

    </table>
@endsection