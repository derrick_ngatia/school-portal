@extends('layouts.admin')
@section('header')
    @endsection
@section('content')
   <form class="form-horizontal" method="POST" action='{{url("/addStudent")}}' enctype="multipart/form-data">
       {{ csrf_field() }}
       <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
           <label for="name" class="col-md-4 control-label">STUDENT NAME</label>

           <div class="col-md-6">
               <input id="name" type="text" class="form-control" value="{{ old('name') }}" name="name" required>

               @if ($errors->has('name'))
                   <span class="help-block">
                       <strong>{{ $errors->first('name') }}</strong>
                   </span>
               @endif
           </div>
       </div>
       <div class="form-group{{ $errors->has('p_email') ? ' has-error' : '' }}">
           <label for="name" class="col-md-4 control-label">Parent Email</label>

           <div class="col-md-6">
               <input id="p_email" type="email" value="{{ old('p_email') }}" class="form-control" name="p_email" required>

               @if ($errors->has('p_email'))
                   <span class="help-block">
                       <strong>{{ $errors->first('p_email') }}</strong>
                   </span>
               @endif
           </div>
       </div>
       <div class="form-group{{ $errors->has('admission') ? ' has-error' : '' }}">
           <label for="admission" class="col-md-4 control-label">ADMISSION NUMBER</label>

           <div class="col-md-6">
               <input id="admission"  value="{{ old('admission') }}" type="number" class="form-control" name="admission" required>

               @if ($errors->has('admission'))
                   <span class="help-block">
                                        <strong>{{ $errors->first('admission') }}</strong>
                                    </span>
               @endif
           </div>
       </div>
       <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
           <label for="year" class="col-md-4 control-label">CLASS</label>

           <div class="col-md-6">
               <input id="year" type="number" value="{{ old('year') }}" class="form-control" name="year" required>

               @if ($errors->has('year'))
                   <span class="help-block">
                                        <strong>{{ $errors->first('year') }}</strong>
                                    </span>
               @endif
           </div>
       </div>
       <div class="form-group{{ $errors->has('term') ? ' has-error' : '' }}">
           <label for="year" class="col-md-4 control-label">TERM</label>

           <div class="col-md-6">
               <input id="term" type="number" value="{{ old('term') }}" class="form-control" name="term" required>

               @if ($errors->has('term'))
                   <span class="help-block">
                                        <strong>{{ $errors->first('term') }}</strong>
                                    </span>
               @endif
           </div>
       </div>
       <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
           <label for="photo" class="col-md-4 control-label">PASSPORT PHOTO</label>

           <div class="col-md-6">
               <input id="photo" type="file" accept="image/*" class="form-control" name="photo" required>

               @if ($errors->has('photo'))
                   <span class="help-block">
                                        <strong>{{ $errors->first('photo') }}</strong>
                                    </span>
               @endif
           </div>
       </div>
       <div class="form-group">
           <div class="col-md-6 col-md-offset-4">
               <button type="submit" class="btn btn-primary">
                   ADD STUDENT
               </button>
           </div>
       </div>
   </form>
    @endsection