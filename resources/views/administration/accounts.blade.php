@extends('layouts.admin');
@section('header')
    Home
@endsection
@section('content')
    <form action='{{url("/search_parent_account")}}' method="post">
        {{csrf_field()}}
        <div class="input-group col-md-5 -align-center">
            <input type="text" name="search" placeholder="search by name" class="form-control"/>
            <span class="input-group-btn">
                <button class="btn btn-default">
                    Go!
                </button>
            </span>
        </div>
    </form>
<table class="table table-stripped">
<thead>
<th>
    Parent Name
</th>
<th>
    Email
</th>
<th>
    Phone Number
</th>
<th>
    Location
</th>
</thead>
    @if (count($user)>0)
        @foreach($user as $innews)
    <tbody>
<tr>
    <td>{{$innews->name}}</td>
    <td>{{$innews->email}}</td>
    <td>{{$innews->phone}}</td>
    <td>{{$innews->location}}</td>
    <td><a href='{{url("/deletesAccount/{$innews->id}")}}'><button class="btn btn-danger btn-sm">Delete Account</button></a>  </td>
</tr>



    </tbody>
        @endforeach
    @else
        <h3>No Accounts Yet</h3>
    @endif
</table>

@endsection