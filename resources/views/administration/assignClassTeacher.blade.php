@extends('layouts.admin')
@section('header')
@endsection
@section('content')
    <form class="form-horizontal" method="POST" action='{{url("/assignTeacher")}}' enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('id') ? ' has-error' : '' }}">
            <label for="id" class="col-md-4 control-label">Assign Teacher</label>

            <div class="col-md-6">
                <select class="form-control"  name="id" id="id">
                    @foreach($teacher->all() as $single)
              <option value="{{$single->staff_no}}">{{$single->name}}</option>

                        @endforeach
                </select>

                @if ($errors->has('id'))
                    <span class="help-block">
                       <strong>{{ $errors->first('id') }}</strong>
                   </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('class') ? ' has-error' : '' }}">
            <label for="class" class="col-md-4 control-label">Class</label>

            <div class="col-md-6">
                <select class="form-control"  name="class" id="class">
                    <option  value="1">Class One</option>
                    <option   value="2">Class Two</option>
                    <option  value="3">Class Three</option>
                    <option   value="4">Class Four</option>
                    <option  value="5">Class Five</option>
                    <option   value="6">Class Six</option>
                    <option  value="7">Class Seven</option>
                    <option   value="8">Class Eight</option>
                </select>
                @if ($errors->has('class'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('class') }}</strong>
                                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    ASSIGN CLASS TEACHER
                </button>
            </div>
        </div>
    </form>
@endsection