@extends('layouts.admin')
@section('header')
@endsection
@section('content')
    <form class="form-horizontal" method="POST" action='{{url("/registerTeacher")}}' enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Teacher Name</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control" value="{{ old('name') }}" name="name" required>

                @if ($errors->has('name'))
                    <span class="help-block">
                       <strong>{{ $errors->first('name') }}</strong>
                   </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Teacher Email</label>

            <div class="col-md-6">
                <input id="email" type="email" value="{{ old('email') }}" class="form-control" name="email" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                       <strong>{{ $errors->first('email') }}</strong>
                   </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
            <label for="gender" class="col-md-4 control-label">Gender</label>

            <div class="col-md-6">
               <select class="form-control"  name="gender" id="gender">
                   <option  value="0">Male</option>
                   <option   value="1">Female</option>
               </select>
                @if ($errors->has('gender'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('staff_no') ? ' has-error' : '' }}">
            <label for="staff_no" class="col-md-4 control-label">Staff No</label>

            <div class="col-md-6">
                <input id="year" type="number" value="{{ old('staff_no') }}" class="form-control" name="staff_no" required>

                @if ($errors->has('staff_no'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('staff_no') }}</strong>
                                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    ADD TEACHER
                </button>
            </div>
        </div>
    </form>
@endsection