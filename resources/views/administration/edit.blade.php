@extends('layouts.admin')
@section('header')
@endsection
@section('content')
    <form class="form-horizontal" method="post" action='{{url("/updateParentAccount/{$user->id}")}}'>
        {{ csrf_field() }}

        <h2 class="text-center">Update Parents Account</h2>
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">Parent Name</label>

            <div class="col-md-6">
                <input id="name" type="name" value="{{$user->name}}" class="form-control" name="name" required>

                @if ($errors->has('name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Email</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" value="{{$user->email}}" name="email" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('county') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">County of Residence</label>

            <div class="col-md-6">
                <input id="county" type="text" class="form-control" value="{{$user->location}}" name="county" required>

                @if ($errors->has('county'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('county') }}</strong>
                                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
            <label for="phone" class="col-md-4 control-label">Phone Number</label>

            <div class="col-md-6">
                <input id="phone" type="number" class="form-control" value="{{$user->phone}}" name="phone" required>

                @if ($errors->has('phone'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Update
                </button>
            </div>
        </div>
    </form>
@endsection