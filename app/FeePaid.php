<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeePaid extends Model
{
    //
    protected $fillable=['s_admission','acc_ref','bank_name','acc_name','acc_no','phone','amount'];
}
