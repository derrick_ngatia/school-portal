<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{

    protected $fillable=['imageurl','admission','student_name','year','term'];
}
