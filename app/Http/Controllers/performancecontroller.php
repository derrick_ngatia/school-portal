<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use App\News;
use Illuminate\Http\Request;
use App\Students;
use App\Performance;
use DateTime;
class performancecontroller extends Controller
{
    //
    function perfomance(){
      $news=News::all();
      return View('perform.perfomance',['news'=>$news]);
    }
   function news(Request $request){
    $this->validate($request,['story'=>'required','date'=>'required','image'=>'required']);
    $news=new News;
    $news->description=$request->input('story');
      $news->app_date=$request->input('date');
      if(Input::hasFile('image')){
        $file=Input::file('image');
        $file->move(public_path(). '/uploads/', $file->getClientOriginalName());
        $url=URL::to("/"). '/uploads/'. $file->getClientOriginalName();
      }
        $news->image=$url;
        $news->save();
        $news=News::all();
        return redirect('/home')->with('response','News has been posted successfully');
   }
   function addscore(){
     $students=Students::all();
     return view('addscore.addscore',['students'=>$students]);
   }
   function newsupdate(Request $request,$id){
    $this->validate($request,['story'=>'required','date'=>'required','image'=>'required']);
    $news=new News;
    $news->id=$id;
    $news->description=$request->input('story');
      $news->app_date=$request->input('date');
      if(Input::hasFile('image')){
        $file=Input::file('image');
        $file->move(public_path(). '/uploads/', $file->getClientOriginalName());
        $url=URL::to("/"). '/uploads/'. $file->getClientOriginalName();
      }
        $news->image=$url;
        $data=array(
          'id'=>$news->id,
          'image'=>$news->image,
          'description'=>$news->description,
          'app_date'=>$news->app_date,
          'created_at'=>$news->created_at,
          'updated_at'=>$news->updated_at,
        );
        News::where('id',$id)->update($data);
        $news->update();
        $news=News::all();
        return redirect('/home')->with('response','News has been posted successfully');
   }
   function postscore(Request $request){
      $students=new Performance;
       $this->validate($request,['students'=>'required','exam_name'=>'required','year'=>'required','term'=>'required','score'=>'required','position'=>'required']);
       $students->student_no=$request->input('students');
       $students->exam_name=$request->input('exam_name');
       $students->year=$request->input('year');
       $students->term=$request->input('term');
       $students->position=$request->input('position');
       $students->score=$request->input('score');
       $students->created_at=new DateTime();
       $students->save();
       return redirect('/home')->with('response','Marks has been posted successfully');

   }
}
