<?php

namespace App\Http\Controllers;

use App\ClassTeacher;
use App\News;
use App\Score;
use App\Students;
use App\Teacher;
use Auth;
use Illuminate\Http\Request;

class ClassTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $news=News::all();
        return view('classteacher.home',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$student_id,$score_id)
    {
        $this->validate($request,['eng'=>'required|integer|between:0,100','math'=>'required|integer|between:0,100','kisw'=>'required|integer|between:0,100',
            'sci'=>'required|integer|between:0,100','ss'=>'required|integer|between:0,100']);
        $score=Score::where('id',$score_id)->first();
        $score->update([
           'math'=>$request->input('math'),
            'eng'=>$request->input('eng'),
            'kisw'=>$request->input('kisw'),
            'sci'=>$request->input('sci'),
            'ss'=>$request->input('ss'),
            'total'=>$request->input('math')+
                $request->input('eng')+
                $request->input('kisw')+
                $request->input('sci')+
                $request->input('ss'),
            'mteacher'=>$request->input('mteacher'),
            'eteacher'=>$request->input('eteacher'),
            'steacher'=>$request->input('steacher'),
            'kteacher'=>$request->input('kteacher'),
            'sciteacher'=>$request->input('sciteacher'),
            'class_teachers_comment'=>$request->input('comment')
        ]);
        flash('updated successfully')->success();
        return redirect()->back();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $staff_no=Teacher::where('email',Auth::user()->email)->first();
        $class=ClassTeacher::where('staff_no',$staff_no->staff_no)->first();
        $students=Students::where('year',$class->class)->get();
        return view('classteacher.students',compact('students'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function student_results(){

        $staff_no=Teacher::where('email',Auth::user()->email)->first();
        $class=ClassTeacher::where('staff_no',$staff_no->staff_no)->first();
        $students=Students::where('year',$class->class)->get();
        return view('classteacher.results',compact('students'));

    }
    public function view_marks($id){
        $student=Students::where('id',$id)->first();
        $score=Score::where('s_id',$id)->get();
        return view('classteacher.score',compact('score','student'));

    }
    public function edit($id)
    {
        //
    }

    public function store_score(Request $request,$id){
        $this->validate($request,['eng'=>'required|integer|between:0,100','math'=>'required|integer|between:0,100',
            'kisw'=>'required|integer|between:0,100',
            'sci'=>'required|integer|between:0,100',
            'ss'=>'required|integer|between:0,100']);
        $students=Students::where('id',$id)->first();
        $score=new Score();
        $total=$request->input('eng')+$request->input('math')+$request->input('kisw')+
        $request->input('sci')+$request->input('ss');
        $score->total=$total;
        $score->s_id=$id;
        $score->class=$students->year;
        $score->term=$students->term;
        $score->eng=$request->input('eng');
        $score->math=$request->input('math');
        $score->kisw=$request->input('kisw');
        $score->sci=$request->input('sci');
        $score->ss=$request->input('ss');
        $score->mteacher=$request->input('mteacher');
        $score->eteacher=$request->input('eteacher');
        $score->kteacher=$request->input('kteacher');
        $score->steacher=$request->input('steacher');
        $score->sciteacher=$request->input('sciteacher');
        $score->class_teachers_comment=$request->input('comment');
        $exists=Score::where('class',$students->year)->where('term',$students->term)->exists();

        if (!$exists){
            $score->save();
            flash('students marks registered successfully')->success();
            return redirect()->back();
        }elseif($exists){
            flash('marks already registered for that class and term')->warning();
            return redirect()->back();
        }else{
            flash('error inserting marks. Check mark score if its greater than the required or less than 0')->error();
            return redirect()->back();
        }

    }
    public  function record($id){
        $student=Students::where('id',$id)->first();
        return view('classteacher.record',compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($student_id,$score_id)
    {
        $student=Students::where('id',$student_id)->first();
        $score=Score::where('id',$score_id)->first();
       return view('classteacher.edit',compact('student','score','score_id'));
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
