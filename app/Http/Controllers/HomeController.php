<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\News;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::check()){
          $email=Auth::user()->email;
          $getStudent = DB::select('select * from students where parent_email = ?',[$email]);
          $getScore = DB::table('performances')->select('performances.score','performances.position','performances.exam_name','performances.year','performances.term')->join('students','students.id','=','performances.student_no')->where('students.parent_email',$email)->get();
          $news=News::all();
          return view('home',['news'=>$news ,'students'=>$getStudent,'scores'=>$getScore,'auth'=>$email]);
      } else {
        $news=News::all();
        return view('home',['news'=>$news]);
      }

    }
    public function delete($id)
    {

      News::where('id',$id)->delete();
      $email=Auth::user()->email;
      $getStudent = DB::select('select * from students where parent_email = ?',[$email]);
      $getScore = DB::table('performances')->select('performances.score','performances.position','performances.exam_name','performances.year','performances.term')->join('students','students.id','=','performances.student_no')->where('students.parent_email',$email)->get();
      $news=News::all();
      return view('home',['news'=>$news ,'students'=>$getStudent,'scores'=>$getScore,'auth'=>$email]);
    }
    public function edit($id)
    {
      $news = News::FindorFail($id);
      return View('edit',['news'=>$news]);
    }
}
