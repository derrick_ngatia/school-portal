<?php

namespace App\Http\Controllers;

use App\ClassTeacher;
use App\Http\Middleware\results;
use App\Mail\WelcomeMail;
use App\News;
use App\Students;

use App\Teacher;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $news=News::all();
        return view('home',['news'=>$news]);
        return view('administration.index',['news'=>$news]);
    }
    public function getStudentForm(){
        return view('administration.studentform');
    }
    public function add_head_teacher(){
        return view('administration.headteacher');    }
    public function add_secretary(){
        return view('administration.add_secretary');

    }
    public function secretary(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|unique:users',
            'phone'=>'required|max:10|min:10|unique:users',
            'location'=>'required'
        ]);
        $user=new User();
        $user->name=$request->input('name');
        $user->email=$request->input('email');
        $user->phone=$request->input('phone');
        $user->location=$request->input('location');
        $user->password=bcrypt($request->input('email'));
        $user->user_type='secretary';
        Mail::to($request->input('email'))->send(new WelcomeMail($user));
        $user->save();
        flash( 'Secretary account created successfully. An email has been sent to the registered user')->success();
        return redirect()->back();
    }
    public function head_teacher(Request $request){
        $this->validate($request,[
            'name'=>'required|regex:/^[A-Za-z\s-_]+$/',
            'email'=>'required|unique:users',
            'phone'=>'required|max:10|min:10|unique:users',
            'location'=>'required'
        ]);
        $user=new User();
        $user->name=$request->input('name');
        $user->email=$request->input('email');
        $user->phone=$request->input('phone');
        $user->location=$request->input('location');
        $user->password=bcrypt($request->input('email'));
        $user->user_type='head_teacher';
        Mail::to($request->input('email'))->send(new WelcomeMail($user));
        $user->save();
        flash( 'HeadTeacher account created successfully. An email has been sent to the registered user')->success();
        return redirect()->back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public  function storeTeacher(Request $request){
        $this->validate($request,
            ['email'=>'required|unique:teachers','name'=>'required|regex:/^[A-Za-z\s-_]+$/','gender'=>'bool|required','staff_no'=>'required|unique:teachers']);
        $teacher=New Teacher();
     $teacher->email=$request->input('email');
     $teacher->name=$request->input('name');
     $teacher->gender=$request->input('gender');
     $teacher->staff_no=$request->input('staff_no');
     $teacher->save();
     flash('Teacher added successfully')->success();
     return redirect()->back();

    }
    public function  update_a(Request $request,$id){
        $this->validate($request,['email'=>'required','phone'=>'required|max:10|min:10']);
        $user=User::where('id',$id)->first();
        $user->update([
           'name'=>$request['name'],
            'email',$request['email'],
            'phone'=>$request['phone'],
            'location'=>$request['county']
        ]);
        $users=User::where('user_type','parent')->get();
        flash('updated successfully')->success();
        return view('administration.parents',compact('users'));

    }
    public  function update_parent($id){
        $user=User::where('id',$id)->first();
        return view('administration.edit',compact('user'));
    }
    public function edit_parent(){
        $users=User::where('user_type','parent')->get();
        return view('administration.parents',compact('users'));
    }
    public function addTeacher()
    {
        return view('administration.addTeacher');
    }
    public function viewStudents(){
        $students=Students::all();
        return view('administration.allstudents')->with(compact('students'));
    }

    public function  delete(){
       $user=User::where('user_type','parent')->get();
        return view('administration.accounts')->with(compact('user'));
    }
    public function  deleteAccount($id){
        User::Where('id',$id)->delete();
        $user=User::all();
        return view('administration.accounts')->with(compact('user'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this
            ->validate($request,['name'=>'required','admission'=>'required|unique:students','year'=>'required|integer|between:1,8','term'=>'required|integer|between:1,3']);
     $student=new Students();
        $student->student_name=$request['name'];
        $student->imageurl="uploads/backie.jpg";
     $student->admission=$request['admission'];
     $student->year=$request['year'];
     $student->term=$request['term'];
     $student->parent_email=$request['p_email'];

     if ($request->hasFile('photo')){
         $extension=$request->file('photo')->getClientOriginalExtension();
         $fileName=rand(11111,99999).'.'.$extension;
         Input::file('photo')->move(storage_path().'/app/public/profiles',$fileName);
         $student->imageurl=$fileName;
     }

        $student->save();
        $students=Students::all();
        flash( 'Student added successfully')->success();
        return view('administration.allstudents')->with(compact('students'));
        //
    }

    public  function assignClassTeacher(){
        $teacher=new Teacher();
        return view('administration.assignClassTeacher',compact('teacher'));

    }
    public  function getAll()
    {
        $classTeachers=Teacher::select('teachers.name','teachers.email','teachers.gender','class_teachers.class','teachers.staff_no')
            ->join('class_teachers',function ($join){
                $join->on('teachers.staff_no','=','class_teachers.staff_no');
            })
            ->get();
        //dd($classTeachers);
        return view('administration.allClassTeachers',compact('classTeachers'));
    }
    public  function assignTeacher(Request $request){
        $this->validate($request,['id'=>'required|unique:class_teachers','class'=>'required|unique:class_teachers']);
     $teacher=Teacher::where('staff_no',$request->input('id'))->first();
     $user=new User();
     $user->name=$teacher->name;
     $user->email=$teacher->email;
     $user->user_type="class_teacher";
     $user->password=bcrypt($teacher->email);
     $user->save();
     $classTecher=new ClassTeacher();
     $classTecher->class=$request->input('class');
     $classTecher->staff_no=$request->input('id');
     $classTecher->save();
     flash('Teacher assigned to class successfully')->success();

        return redirect()->back();


    }
    public function search_parent_account(Request $r){
        $user=User::where('user_type','parent')->where('name','LIKE','%'.$r->input('search').'%')
            ->get();
        return view('administration.accounts')->with(compact('user'));
    }
    public function search_student(Request $r)
    {
        $students=Students::where('admission','LIKE','%'.$r->input('search').'%')->get();
        return view('administration.allstudents')->with(compact('students'));
    }
    public function search_class_teacher(Request $r)
    {
        $classTeachers=Teacher::select('teachers.name','teachers.email','teachers.gender','class_teachers.class','teachers.staff_no')
            ->join('class_teachers',function ($join){
                $join->on('teachers.staff_no','=','class_teachers.staff_no');
            })->where('teachers.name','LIKE','%'.$r->input('search').'%')
            ->get();
        //dd($classTeachers);
        return view('administration.allClassTeachers',compact('classTeachers'));
    }    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public  function parentAccount(Request $r){
        $this->validate($r,['name'=>'required','email'=>'required|unique:users','phone'=>'required|max:10|unique:users','county'=>'required']);
        if (Students::where('parent_email',$r->input('email'))->exists()){
            $user=new User();
            $user->name=$r->input('name');
            $user->email=$r->input('email');
            $user->phone=$r->input('phone');
            $user->location=$r->input('county');
            $user->password=bcrypt($r->input('email'));
            $user->save();
            Mail::to($r->input('email'))->send(new WelcomeMail($user));
            flash( 'parent account created successfully. An email has been sent to the registered user')->success();
            return view('administration.parentAccountform');
        }
        else{
            flash('No student is registered with that email')->error();
            return view('administration.parentAccountform');

        }



    }
     public  function parentForm(){

      return view('administration.parentAccountform');
     }
    /**
     *
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($staff_no)
    {
        //
        $teacher=ClassTeacher::where('staff_no',$staff_no)->first();
       // dd($teacher);
        return view('administration.update_class_teacher',compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,['staff_no'=>'required','class'=>'required|integer|min:1|max:8']);
        $update=ClassTeacher::where('id',$id)->update([
           'class'=>$request->input('class'),
       ]);
       if ($update){
           $classTeachers=Teacher::select('teachers.name','teachers.email','teachers.gender','class_teachers.class','teachers.staff_no')
               ->join('class_teachers',function ($join){
                   $join->on('teachers.staff_no','=','class_teachers.staff_no');
               })
               ->get();
           //dd($classTeachers);
           return view('administration.allClassTeachers',compact('classTeachers'));
       }
       else{
           flash('error')->error();
       }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       $delete=ClassTeacher::where('staff_no',$id)->delete();
       if($delete){
           $classTeachers=Teacher::select('teachers.name','teachers.email','teachers.gender','class_teachers.class','teachers.staff_no')
               ->join('class_teachers',function ($join){
                   $join->on('teachers.staff_no','=','class_teachers.staff_no');
               })
               ->get();
           //dd($classTeachers);
           flash('class teacher deleted')->success();
           return view('administration.allClassTeachers',compact('classTeachers'));
       }
    }
}
