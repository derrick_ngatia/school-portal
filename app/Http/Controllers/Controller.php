<?php

namespace App\Http\Controllers;

use App\Mail\DemoMail;
use App\User;
use Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Mail;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function help(){
        return view('help');
    }
    public function post(Request $request){
          $user=Auth::User();
          $admin=User::where('user_type','admin')->first();
          Mail::to($admin->email)->send(new DemoMail($user,$request));
          flash('Request sent. Our support team will get back to you shortly via your email. Thank You')->success();
          return redirect()->back();

    }
}
