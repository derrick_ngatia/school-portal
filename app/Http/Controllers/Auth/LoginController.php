<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function authenticated(Request $request, $user)
    {
        if(($user->user_type==='admin')){
            return redirect()->intended('home');

        }
        if(($user->user_type==='class_teacher')){
            return redirect()->intended('class_teacher');

        }
        if(($user->user_type==='head_teacher')){
            return redirect()->intended('/headteacher');

        }
        if(($user->user_type==='secretary')){
            return redirect()->intended('/secretary');

        }
        if(($user->user_type==='parent')){
            return redirect()->intended('/parentshome');

        }
        return redirect('/error');
    }

    //protected $redirectTo =  '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
