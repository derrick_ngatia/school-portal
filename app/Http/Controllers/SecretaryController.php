<?php

namespace App\Http\Controllers;

use App\FeePaid;
use App\Score;
use App\Secretary;
use App\Students;
use App\User;
use Cmgmyr\Messenger\Models\Thread;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SecretaryController extends Controller
{
    //
    public function index(){

        return view('secretary.home',compact('users'));
    }
    public function parents(){
        $users=User::where('user_type','parent')->get();
        return $users;
    }
    public function message_form($id){
        return view('messenger.partials.form-message',compact('id'));
    }
    public function fee_payment(){
      return view('secretary.fee_form');
    }
    public function record_fee_payment(Request $request){
        $this->validate($request,['acc_ref'=>'string|max:30','acc_name'=>'string|max:30','phone'=>'string|max:10|min:10','amount'=>'integer|min:0']);
        $student=Students::where('admission',$request->input('admission'))->exists();
        if ($student){
            $fee_paid=new FeePaid();
            $fee_paid->s_admission=$request->input('admission');
            $fee_paid->acc_ref=$request->input('acc_ref');
            $fee_paid->bank_name=$request->input('bank_name');
            $fee_paid->acc_name=$request->input('acc_name');
            $fee_paid->acc_no=$request->input('acc_no');
            $fee_paid->phone=$request->input('phone');
            $fee_paid->amount=$request->input('amount');
            $fee_paid->save();
            flash('fee recorded')->success();
            return redirect()->back();
        }else{
            flash('No such student found')->error();
            return redirect()->back();
        }


    }
    public function report_forms(){
        $students=DB::table('students')
            ->orderBy('year', 'desc')
            ->orderBy('term', 'desc')
            ->get();
        return view('secretary.students',compact('students'));
    }
    public function search_report_form(Request $request){
        $students=Students::where('admission','LIKE','%'.$request->input('search').'%')
            ->orderBy('year', 'desc')
            ->orderBy('term', 'desc')
            ->get();
        return view('secretary.students',compact('students'));
    }
    public  function student_report_form($id){
        $student=Students::where('id',$id)->first();
        return view('secretary.report',compact('student'));
    }
    public  function generate(Request $r,$id){
        $student=Students::where('id',$id)->first();
        $item=Score::where('s_id',$student->id)->where('class',$r->input('class'))->where('term',$r->input('term'))->first();
       if ($item){
            $pdf=PDF::loadView('reports.reportform',compact('student','item'));
           return $pdf->download(str_slug($student->name+"_"+$student->admission)+'invoice.pdf');

       }
       else{
           flash('no results available for that class and term')->error();
           return redirect()->back();
       }

    }
    public function news(){
        $news=DB::table('news')
            ->latest()
            ->get();
        return view('secretary.news',compact('news'));
    }
    public function fee_structure(){
        $secretary=Secretary::all()->first();
        if ($secretary){
            $secretary=Secretary::all()->first();
            return view('secretary.structure',compact('secretary'));

        }else {
            return view('secretary.fee_structure');

        }
    }
    public function post_fee_structure(Request $request){
            $secretary=new Secretary();
            $secretary->class_1=$request->input('class_1');
            $secretary->class_2=$request->input('class_2');
            $secretary->class_3=$request->input('class_3');
            $secretary->class_4=$request->input('class_4');
            $secretary->class_5=$request->input('class_5');
            $secretary->class_6=$request->input('class_6');
            $secretary->class_7=$request->input('class_7');
            $secretary->class_8=$request->input('class_8');
            $secretary->notes=$request->input('notes');
            $secretary->enrollment=$request->input('enrollment');
            $secretary->caution=$request->input('caution');
            $secretary->save();
            flash('fee structure saved')->success();
            return redirect()->back();



    }
    public function edit_fee_structure(){
        $secretary=Secretary::all()->first();
        return view('secretary.edit',compact('secretary'));
    }
    public function update_fee_structure(Request $request){
        $secretary=Secretary::all()->first();
        $secretary->class_1=$request->input('class_1');
        $secretary->class_2=$request->input('class_2');
        $secretary->class_3=$request->input('class_3');
        $secretary->class_4=$request->input('class_4');
        $secretary->class_5=$request->input('class_5');
        $secretary->class_6=$request->input('class_6');
        $secretary->class_7=$request->input('class_7');
        $secretary->class_8=$request->input('class_8');
        $secretary->notes=$request->input('notes');
        $secretary->enrollment=$request->input('enrollment');
        $secretary->caution=$request->input('caution');
        $secretary->update();
        flash('fee structure updated')->success();
        return redirect()->back();
    }
    public function news_letter(){

    }

}
