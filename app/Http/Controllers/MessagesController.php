<?php
namespace App\Http\Controllers;
use App\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
class MessagesController extends Controller
{
    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index()
    {
        // All threads, ignore deleted/archived participants
        $threads = Thread::getAllLatest()->get();
        // All threads that user is participating in
        // $threads = Thread::forUser(Auth::id())->latest('updated_at')->get();
        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')->get();
        return view('secretary.home', compact('threads'));
    }
    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            return redirect()->route('messages');
        }
        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();
        // don't show the current user in list
        $userId = Auth::id();
        $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();
        $thread->markAsRead($userId);
        return view('messenger.show', compact('thread', 'users'));
    }
    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create()
    {
        $users = User::where('id', '!=', Auth::id())->get();
        return view('messenger.create', compact('users'));
    }
    public  function fetchUnread($id){
        $unreadMessages = 0;
        $exists=Participant::where('id',$id)->exists();

        if ($exists) {

            $threads = Thread::forUser($id)->latest('updated_at')->get();

            foreach ($threads as $thread) {
                $unread_count = $thread->userUnreadMessagesCount($id);
                $unreadMessages += $unread_count;
            }

            return response()->json($unreadMessages);
        }else{
            return 0;
        }

    }
    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store($id){
        $result=Participant::where('user_id',$id)->first();
        if ($result!=null){
            $exist=Participant::where('thread_id',$result->thread_id)->where('user_id',Auth::id())->exists();
            if ($exist){
                $thread=Thread::where('id',$result->thread_id)->first();
                $thread->markAsRead(Auth::id());
                $messages=Message::where('thread_id',$result->thread_id)->get();
                return $messages;
            }
        }else{
            return null;
        }

    }
    public function store1(Request $request,$id)
    {
        $result=Participant::where('user_id',$id)->first();
        if ($result!=null){
            $exist=Participant::where('thread_id',$result->thread_id)->where('user_id',Auth::id())->exists();
            if ($exist){
                $thread=Thread::where('id',$result->thread_id)->first();
                $thread->markAsRead(Auth::id());
                $this->update($result->thread_id);
                $messages=Message::where('thread_id',$result->thread_id)->get();
                return $messages;
            }
        }else{
            $thread = Thread::create([
                'subject' => Auth::user()->name,
            ]);
            // Message
            Message::create([
                'thread_id' => $thread->id,
                'user_id' => Auth::id(),
                'body' => $request->input('message'),
            ]);
            // Sender
            Participant::create([
                'thread_id' => $thread->id,
                'user_id' => Auth::id(),
                'last_read' => new Carbon,
            ]);
            Participant::create([
                'thread_id' => $thread->id,
                'user_id' => $id,
                'last_read' => new Carbon,
            ]);
            $messages=Message::where('thread_id',$thread->id)->latest()->get();
            return $messages;
        }

    }
    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            return redirect()->back();
        }
        $thread->activateAllParticipants();
        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => Input::get('message'),
        ]);
        // Add replier as a participant
        $participant = Participant::firstOrCreate([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
        ]);
        $participant->last_read = new Carbon;
        $participant->save();
        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant(Input::get('recipients'));
        }
        $messages=Message::where('thread_id',$id)->get();
        return $messages;
    }
    public function id(){
        return Auth::user()->id;
    }
}
