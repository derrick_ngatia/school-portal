<?php

namespace App\Http\Controllers;

use App\Discpline;
use App\FeePaid;
use App\News;
use App\Score;
use App\Secretary;
use App\Students;
use PDF;
use Illuminate\Http\Request;
use Auth;
class ParentsController extends Controller
{
    //
    public function  chat()
    {
        return view('parents.chat');
    }
    public function index()
    {
        $news = News::all()->forPage(1, 5);
        return view('parents.home', compact('news'));
    }

    public function child()
    {
        $students = Students::where('parent_email', Auth::user()->email)->get();
        return view('parents.results', compact('students'));

    }

    public function results($id)
    {
        $student = Students::where('id', $id)->first();
        $scores = Score::where('s_id', $id)
            ->latest()
            ->get();
        return view('parents.results_profile', compact('student', 'scores'));

    }

    public function fee_structure()
    {
        $secretary = Secretary::all()->first();
        return view('parents.structure', compact('secretary'));
    }

    public function print_fee_structure_parent()
    {
        $secretary = Secretary::all()->first();
        $pdf = PDF::loadView('reports.structure', compact('secretary'));
        return $pdf->download('fee_structure.pdf');

    }

    public function fee_paid()
    {
        $students = Students::where('parent_email', Auth::user()->email)->get();
        return view('parents.fee', compact('students'));

    }

    public function fee($id)
    {
        $student = Students::where('id', $id)->first();
        $fees = FeePaid::where('s_admission', $student->admission)->get();
        return view('parents.receipt', compact('fees', 'student'));

    }

    public function fee_receipt($id)
    {

        $fee = FeePaid::where('id', $id)->first();
        $student = Students::where('admission', $fee->s_admission)->first();
        $pdf = PDF::loadView('reports.fee_recept', compact('fee', 'student'));
        return $pdf->download('fee_receipt.pdf');

    }

    public function print_report_form_score($id)
    {

        $item = Score::where('id',$id)->first();
        $student = Students::where('id', $item->s_id)->first();
        $pdf = PDF::loadView('reports.reportform', compact('student', 'item'));
        return $pdf->download( 'report_form.pdf');

    }
    public function mykids(){
        $students = Students::where('parent_email', Auth::user()->email)->get();
        return view('parents.kids', compact('students'));
    }
    public function disciplinary($id){
        $cases=Discpline::where('s_id',$id)->get();
        return view('parents.cases',compact('cases'));
    }
}
