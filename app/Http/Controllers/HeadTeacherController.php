<?php

namespace App\Http\Controllers;

use App\Discpline;
use App\News;
use App\Score;
use App\Students;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HeadTeacherController extends Controller
{
    //
    public function index(){
        $students=DB::table('students')
            ->orderBy('year', 'desc')
            ->orderBy('term', 'desc')
            ->get();
        return view('head_teacher.home',compact('students'));
    }
    public function  search_student(Request $request){
        $students=Students::where('admission','LIKE','%'.$request->input('search').'%')
            ->orderBy('year', 'desc')
            ->orderBy('term', 'desc')
            ->get();
        return view('head_teacher.home',compact('students'));
    }
    public  function  profile($id){
        $student=Students::where('id',$id)->first();
        $cases=Discpline::where('s_id',$id)->get();
        $score=Score::where('s_id',$student->id)->where('class',$student->year)->where('term',$student->term)->first();
        return view('head_teacher.profile',compact('student','score','cases'));
    }
    public  function head_teacher_comment(Request $request,$id){
        $page = Score::where('id',$id)->first();
        if ($page){
            $page->dPrincipals_comment=$request->input('comment');
            $page->save();
            flash('comment added successfully')->success();
            return redirect()->back();
        }else{
            flash('student marks not found')->error();
        }
    }
    public  function  pending(){
        $cases=Students::select('discplines.id as c_id','students.id','students.admission','students.student_name','discplines.class','discplines.term',
            'discplines.offense','discplines.disciplinary_action','discplines.warning','discplines.cleared','discplines.created_at')
            ->join('discplines',function ($join){
                $join->on('discplines.s_id','=','students.id');
            })
            ->where('discplines.cleared','=',1)
            ->get();
     return view('head_teacher.pending',compact('cases'));
    }
    public  function case_form(){
        return view('head_teacher.caseForm');
    }
    public  function register_case(Request $request){
        $student=Students::where('admission',$request->input('admission'))->first();
        if ($student)
        {
            $case=new Discpline();
            $case->s_id=$student->id;
            $case->class=$student->year;
            $case->term=$student->term;
            $case->offense=$request->input('offense');
            $case->disciplinary_action=$request->input('disciplinary_action');
            $case->warning=$request->input('warning');
            $case->cleared=1;
            $case->save();
            flash('case registered successfully')->success();
            return redirect()->back();
        }else{
            flash('No student with such admission please enter a correct ID')->error();
            return redirect()->back();
        }
    }
    public  function  clear_case($id,$cid){
        $case=Discpline::where('id',$cid)->where('s_id',$id)->update([
            'cleared'=>0
        ]);
        if ($case){
            flash('case cleared')->success();
            return redirect()->back();
        }
        else{
            flash('case clearing error')->error();
            return redirect()->back();
        }

    }
    public function school_disciplinary_actions (){
        return view('head_teacher.categories');
    }
    public function schools_events(){
        $news=DB::table('news')
            ->latest()
            ->get();
        return view('head_teacher.news',compact('news'));
    }
    public  function search_student_c(Request $request){
        $cases=Students::select('discplines.id as c_id','students.id','students.admission','students.student_name','discplines.class','discplines.term',
            'discplines.offense','discplines.disciplinary_action','discplines.warning','discplines.cleared','discplines.created_at')
            ->join('discplines',function ($join){
                $join->on('discplines.s_id','=','students.id');
            })
            ->where('discplines.cleared','=',1)
            ->where('students.admission','LIKE','%'.$request->input('search').'%')
            ->get();
        return view('head_teacher.pending',compact('cases'));
    }
}
