<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    //

    protected $fillable=['s_id','class','term','math','eng','kisw','sci','ss','total','mteacher','kteacher','eteacher','sciteacher','steacher'];
}
