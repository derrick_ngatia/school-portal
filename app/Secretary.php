<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Secretary extends Model
{
    //
    protected $fillable=['class_1','class_2','class_3','class_4','class_5','class_6','class_7','class_8','notes','enrollment','caution'];

}
